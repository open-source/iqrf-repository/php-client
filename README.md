# IQRF Repository integration

[![Build Status](https://gitlab.iqrf.org/open-source/iqrf-repository/php-client/badges/master/pipeline.svg)](https://gitlab.iqrf.org/open-source/iqrf-repository/php-client/-/commits/master)
[![Test coverage](https://gitlab.iqrf.org/open-source/iqrf-repository/php-client/badges/master/coverage.svg)](https://gitlab.iqrf.org/open-source/iqrf-repository/php-client/-/commits/master)
[![Packagist Version](https://img.shields.io/packagist/v/iqrf/iqrf-repository)](https://packagist.org/packages/iqrf/iqrf-repository)
[![Packagist Downloads](https://img.shields.io/packagist/dm/iqrf/iqrf-repository)](https://packagist.org/packages/iqrf/iqrf-repository)
[![Apache License](https://img.shields.io/badge/license-APACHE2-blue.svg)](LICENSE)
[![API documentation](https://img.shields.io/badge/docs-api-brightgreen.svg)](https://apidocs.iqrf.org/iqrf-gateway-webapp-utils/iqrf-repository/)


IQRF Repository API integration for Nette framework.

## Installation

```bash
composer require iqrf/iqrf-repository
```

## Configuration

```neon
extensions:
	iqrfRepository: Iqrf\Repository\DI\IqrfRepositoryExtension

iqrfRepository:
	apiEndpoint: https://repository.iqrfalliance.org/api
```
## License

This project is licensed under Apache License 2.0:

```
 Copyright 2019-2024 IQRF Tech s.r.o.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```
