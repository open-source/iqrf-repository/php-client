<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\DI;

use Contributte\Tester\Environment;
use GuzzleHttp\Client;
use Iqrf\Repository\Configuration;
use Iqrf\Repository\DI\IqrfRepositoryExtension;
use Iqrf\Repository\Entities\Credentials;
use Iqrf\Repository\Models\CompanyManager;
use Iqrf\Repository\Models\FilesManager;
use Iqrf\Repository\Models\ManufacturerManager;
use Iqrf\Repository\Models\OsAndDpaManager;
use Iqrf\Repository\Models\PackageManager;
use Iqrf\Repository\Models\ProductManager;
use Iqrf\Repository\Models\ServerManager;
use Iqrf\Repository\Models\StandardManager;
use Iqrf\Repository\Utils\ApiClient;
use Iqrf\Repository\Utils\FileClient;
use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use Tester\Assert;
use Tester\TestCase;
use function assert;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for Nette DI extension for IQRF Repository API integration
 */
class IqrfRepositoryExtensionTest extends TestCase {

	/**
	 * @var IqrfRepositoryExtension Nette DI extension for IQRF Repository API integration
	 */
	private IqrfRepositoryExtension $extension;

	/**
	 * @var Client HTTP client
	 */
	private Client $httpClient;

	/**
	 * Tests DI extension without the configuration
	 */
	public function testDiWithoutConfiguration(): void {
		$container = $this->createContainer();
		$credentials = new Credentials();
		$configuration = new Configuration(
			apiEndpoint: 'https://repository.iqrfalliance.org/api',
			credentials: $credentials,
		);
		$apiClient = new ApiClient(
			configuration: $configuration,
			httpClient: $this->httpClient,
		);
		$fileClient = new FileClient(
			configuration: $configuration,
			httpClient: $this->httpClient,
		);
		$classes = [
			Configuration::class => $configuration,
			ApiClient::class => $apiClient,
			FilesManager::class => new FilesManager($fileClient),
			CompanyManager::class => new CompanyManager($apiClient),
			ManufacturerManager::class => new ManufacturerManager($apiClient),
			OsAndDpaManager::class => new OsAndDpaManager($apiClient),
			PackageManager::class => new PackageManager($apiClient),
			ProductManager::class => new ProductManager($apiClient),
			ServerManager::class => new ServerManager($apiClient),
			StandardManager::class => new StandardManager($apiClient),
		];

		foreach ($classes as $class => $expected) {
			Assert::equal($expected, $container->getByType($class));
		}
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->extension = new IqrfRepositoryExtension();
		$this->httpClient = new Client();
	}

	/**
	 * Creates Nette DI container
	 *
	 * @param array<string, mixed> $config Configuration
	 * @return Container Nette DI container
	 */
	private function createContainer(array $config = []): Container {
		$tempDir = Environment::getTmpDir();
		$loader = new ContainerLoader($tempDir, true);
		$configuration = array_merge_recursive([
			'parameters' => [
				'appDir' => $tempDir . '/app',
				'tempDir' => $tempDir . '/tmp',
			],
			'services' => [
				Client::class,
			],
		], $config);
		$class = $loader->load(function (Compiler $compiler) use ($configuration): void {
			$compiler->addExtension('iqrfRepository', $this->extension);
			$compiler->addConfig($configuration);
		});

		$container = new $class();
		assert($container instanceof Container);
		return $container;
	}

}

$test = new IqrfRepositoryExtensionTest();
$test->run();
