<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository;

use Iqrf\Repository\Configuration;
use Iqrf\Repository\Entities\Credentials;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/bootstrap.php';

/**
 * Tests for IQRF Repository API integration configuration
 */
class ConfigurationTest extends TestCase {

	private const API_ENDPOINT_URL = 'https://repository.iqrfalliance.org/api';

	/**
	 * @var Configuration IQRF Repository API integration configuration
	 */
	private Configuration $configuration;

	/**
	 * @var Credentials IQRF Repository credentials
	 */
	private Credentials $credentials;

	/**
	 * Tests the function to get IQRF Repository API endpoint URL
	 */
	public function testGetApiEndpoint(): void {
		Assert::same(self::API_ENDPOINT_URL, $this->configuration->getApiEndpoint());
	}

	/**
	 * Tests the function to get credentials
	 */
	public function testGetCredentials(): void {
		Assert::equal($this->credentials, $this->configuration->getCredentials());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->credentials = new Credentials();
		$this->configuration = new Configuration(self::API_ENDPOINT_URL, $this->credentials);
	}

}

$test = new ConfigurationTest();
$test->run();
