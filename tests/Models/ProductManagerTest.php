<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Product;
use Iqrf\Repository\Enums\RfModes;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ProductNotFound;
use Iqrf\Repository\Models\ProductManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for product manager
 */
class ProductManagerTest extends ApiTestCase {

	/**
	 * @var ProductManager Product manager
	 */
	private ProductManager $manager;

	/**
	 * @var array<Product> Products
	 */
	private array $products;

	/**
	 * Tests the function to list all products
	 */
	public function testList(): void {
		$this->mockRequest('products', 'products/data');
		Assert::equal($this->products, $this->manager->list());
	}

	/**
	 * Tests the function to get the product by its ID
	 */
	public function testGet(): void {
		$this->mockRequest('products/0', 'products/0');
		Assert::equal($this->products[0], $this->manager->get(0));
	}

	/**
	 * Tests the function to get the product by its ID (product ot found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('products/9999', 'products/0', null, new DataNotFound());
		Assert::throws(function (): void {
			$this->manager->get(9999);
		}, ProductNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new ProductManager($this->apiClient);
		$this->products = [
			new Product(0, 'Plain DPA without Custom DPA Handler', 0, 'IQRF Tech s.r.o.', 'https://www.iqrfalliance.org/marketplace/plain-dpa-wihtout-custom-dpa-handler', 'https://repository.iqrfalliance.org/productpictures/0000.jpg', RfModes::STD_LP),
			new Product(1, 'Protronix NLII-CO2+RH+T-IQRF+', 1, 'Protronix s.r.o.', 'https://www.iqrfalliance.org/marketplace/nlii-co2-rh-combined-sensor', 'https://repository.iqrfalliance.org/productpictures/0001.png', RfModes::STD),
			new Product(2, 'DDC-SE-01 sensor development kit', 2, 'IQRF Tech s.r.o.', 'https://www.iqrfalliance.org/marketplace/ddc-se-01-sensor-development-kit', 'https://repository.iqrfalliance.org/productpictures/0002.png', RfModes::STD),
		];
	}

}

$test = new ProductManagerTest();
$test->run();
