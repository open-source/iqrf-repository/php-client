<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Os;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Enums\TransceiverFamily;
use Iqrf\Repository\Exceptions\OsNotFound;
use Iqrf\Repository\Models\OsManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF OS build manager
 */
class OsManagerTest extends ApiTestCase {

	/**
	 * @var OsManager IQRF OS build manager
	 */
	private OsManager $manager;

	/**
	 * @var TransceiverFamily Transceiver family
	 */
	private TransceiverFamily $trFamily = TransceiverFamily::TR_7xD;

	/**
	 * @var array<Os> IQRF OS builds
	 */
	private array $builds;

	/**
	 * Tests the function to list all available IQRF OS builds
	 */
	public function testListAll(): void {
		$this->mockRequest('os', 'os/all', []);
		Assert::equal($this->builds, $this->manager->list());
	}

	/**
	 * Tests the function to list all available IQRF OS beta builds
	 */
	public function testListBeta(): void {
		$attributes = new OsDpaAttributes(true, null);
		$this->mockRequest('os', 'os/beta', ['beta' => 'true']);
		$expected = [$this->builds[2]];
		Assert::equal($expected, $this->manager->list($attributes));
	}

	/**
	 * Tests the function to list all available IQRF OS obsolete beta versions
	 */
	public function testListObsoleteBeta(): void {
		$attributes = new OsDpaAttributes(true, true);
		$this->mockRequest('os', 'emptyArray', ['beta' => 'true', 'obsolete' => 'true']);
		Assert::throws(function () use ($attributes): void {
			$this->manager->list($attributes);
		}, OsNotFound::class);
	}

	/**
	 * Tests the function to list all available IQRF OS stable builds
	 */
	public function testListStable(): void {
		$attributes = new OsDpaAttributes(false, false);
		$this->mockRequest('os', 'os/stable', ['beta' => 'false', 'obsolete' => 'false']);
		$expected = [$this->builds[0], $this->builds[1]];
		Assert::equal($expected, $this->manager->list($attributes));
	}

	/**
	 * Tests the function to get the IQRF OS build
	 */
	public function testGet(): void {
		$this->mockRequest('os', 'os/08D5', ['build' => '08D5']);
		Assert::equal($this->builds[1], $this->manager->get('08D5'));
	}

	/**
	 * Tests the function to get the IQRF OS build (IQRF OS build not found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('os', 'emptyArray', ['build' => 'nonsence']);
		Assert::throws(function (): void {
			$this->manager->get('nonsence');
		}, OsNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new OsManager($this->apiClient);
		$this->builds = [
			new Os(
				build: '08C8',
				version: '4.03D',
				trFamily: $this->trFamily,
				attributes: OsDpaAttributes::fromInt(0),
				downloadPath: 'https://repository.iqrfalliance.org/download/iqrfos/08C8',
			),
			new Os(
				build: '08D5',
				version: '4.04D',
				trFamily: $this->trFamily,
				attributes: OsDpaAttributes::fromInt(0),
				downloadPath: 'https://repository.iqrfalliance.org/download/iqrfos/08D5',
			),
			new Os(
				build: '08D7',
				version: '4.05D',
				trFamily: $this->trFamily,
				attributes: OsDpaAttributes::fromInt(1),
				downloadPath: 'https://repository.iqrfalliance.org/download/iqrfos/08D7',
			),
		];
	}

}

$test = new OsManagerTest();
$test->run();
