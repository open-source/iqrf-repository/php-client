<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\Package;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\PackageNotFound;
use Iqrf\Repository\Models\PackageManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for package manager
 */
class PackageManagerTest extends ApiTestCase {

	/**
	 * @var PackageManager Package manager
	 */
	private PackageManager $manager;

	/**
	 * Tests the function to get the package (package not found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('packages/-1', 'emptyArray', null, new DataNotFound());
		Assert::throws(function (): void {
			$this->manager->get(-1);
		}, PackageNotFound::class);
	}

	/**
	 * Tests the function to list packages
	 */
	public function testList(): void {
		$url = 'https://repository.iqrfalliance.org/download/handlers/0002_0001_DDC-SE01.hex';
		$hash = 'A7EDA10634B8BB0C580A9316EC39B8678DCD8A161A3FA3DA5832D382224F7E17';
		$notes = 'Implemented iqrf.sensor.FinalizeSensor? functions.';
		$expected = [
			new Package(66, 2, 1, $url, $hash, '08C8', new DpaVersion('0303'), $notes),
			new Package(98, 2, 1, $url, $hash, '08C8', new DpaVersion('0400'), $notes),
			new Package(141, 2, 1, $url, $hash, '08C8', new DpaVersion('0401'), $notes),
			new Package(179, 2, 1, $url, $hash, '08C8', new DpaVersion('0402'), $notes),
			new Package(205, 2, 1, $url, $hash, '08C8', new DpaVersion('0403'), $notes),
		];
		$this->mockRequest('packages', 'packages/data', ['hwpid' => 2, 'hwpidVer' => 1, 'os' => '08C8']);
		Assert::equal($expected, $this->manager->list(2, 1, '08C8'));
	}

	/**
	 * Tests the function to list packages
	 */
	public function testListOne(): void {
		$url = 'https://repository.iqrfalliance.org/download/handlers/0002_0001_DDC-SE01.hex';
		$hash = 'A7EDA10634B8BB0C580A9316EC39B8678DCD8A161A3FA3DA5832D382224F7E17';
		$notes = 'Implemented iqrf.sensor.FinalizeSensor? functions.';
		$expected = [
			new Package(205, 2, 1, $url, $hash, '08C8', new DpaVersion('0403'), $notes),
		];
		$this->mockRequest('packages', 'packages/205', ['hwpid' => 2, 'hwpidVer' => 1, 'os' => '08C8', 'dpa' => '0403']);
		Assert::equal($expected, $this->manager->list(2, 1, '08C8', '0403'));
	}

	/**
	 * Tests the function to list packages (unknown HWPID)
	 */
	public function testListUnknownHwpId(): void {
		$this->mockRequest('packages', 'emptyArray', ['hwpid' => 65535]);
		Assert::throws(function (): void {
			$this->manager->list(65535);
		}, PackageNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new PackageManager($this->apiClient);
	}

}

$test = new PackageManagerTest();
$test->run();
