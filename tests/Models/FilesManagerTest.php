<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use BadMethodCallException;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Iqrf\Repository\Configuration;
use Iqrf\Repository\Entities\Credentials;
use Iqrf\Repository\Entities\File;
use Iqrf\Repository\Entities\Files;
use Iqrf\Repository\Models\FilesManager;
use Iqrf\Repository\Utils\FileClient;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for Files manager
 */
class FilesManagerTest extends TestCase {

	/**
	 * Base path
	 */
	private const PATH = 'https://repository.iqrfalliance.org/download/dpa/4.03/';

	/**
	 * Tests the function to download the file (failure)
	 */
	public function testDownloadFailure(): void {
		$manager = $this->createManager(['4.03/__files__.json', '4.03/DPA-Coordinator-SPI-7xD-V403-190612.iqrf']);
		Assert::null($manager->download('nonsense'));
	}

	/**
	 * Tests the function to download the file
	 */
	public function testDownload(): void {
		$manager = $this->createManager(['4.03/__files__.json', '4.03/DPA-Coordinator-SPI-7xD-V403-190612.iqrf']);
		$expected = FileSystem::read(__DIR__ . '/../data/files/4.03/DPA-Coordinator-SPI-7xD-V403-190612.iqrf');
		Assert::equal($expected, $manager->download('DPA-Coordinator-SPI-7xD-V403-190612.iqrf'));
	}

	/**
	 * Tests the function to get the file by its name
	 */
	public function testGet(): void {
		$manager = $this->createManager(['4.03/__files__.json', '4.03/__files__.json']);
		$expected = new File(
			name: './DPA-Coordinator-SPI-7xD-V403-190612.iqrf',
			size: 16187,
			date: (new DateTime())->setTimestamp(1560338342),
			sha256: '632351d02aa35038299a198f6cd70be6039d876f5ecadc2822db372bd99619ec',
		);
		Assert::equal($expected, $manager->get('DPA-Coordinator-SPI-7xD-V403-190612.iqrf'));
		Assert::null($manager->get('nonsense'));
	}

	/**
	 * Tests the function to list all files in the directory (failure)
	 */
	public function testListFailure(): void {
		$manager = $this->createManager(['4.03/__files__.json'], null);
		Assert::throws(static function () use ($manager): void {
			$manager->list();
		}, BadMethodCallException::class);
	}

	/**
	 * Tests the function to list all files in the directory
	 */
	public function testList(): void {
		$manager = $this->createManager(['4.03/__files__.json']);
		$json = Json::decode(FileSystem::read(__DIR__ . '/../data/files/4.03/__files__.json'), forceArrays: true);
		$expected = Files::fromApiResponse($json);
		Assert::equal($expected, $manager->list());
	}

	/**
	 * Creates Files manager with mocked HTTP(S) client
	 * @param array<string> $files File names to mock
	 * @return FilesManager Files manager
	 */
	private function createManager(array $files, ?string $path = self::PATH): FilesManager {
		$client = $this->mockClient($files);
		$credentials = new Credentials('username', 'password');
		$config = new Configuration('https://repository.iqrfalliance.org/api', $credentials);
		$httpClient = new FileClient($config, $client);
		$manager = new FilesManager($httpClient, false);
		if ($path !== null) {
			$manager->setPath($path);
		}
		return $manager;
	}

	/**
	 * Mocks HTTP(S) client
	 * @param array<string> $files File names
	 * @return Client Mocked HTTP client
	 */
	private function mockClient(array $files): Client {
		$queue = [];
		foreach ($files as $file) {
			$content = FileSystem::read(__DIR__ . '/../data/files/' . $file);
			$queue[] = new Response(200, [], $content);
		}
		$handler = new MockHandler($queue);
		return new Client(['handler' => HandlerStack::create($handler)]);
	}

}

$test = new FilesManagerTest();
$test->run();
