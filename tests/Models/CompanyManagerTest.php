<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Company;
use Iqrf\Repository\Exceptions\CompanyNotFound;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Models\CompanyManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for the company manager
 */
class CompanyManagerTest extends ApiTestCase {

	/**
	 * @var array<Company> Companies
	 */
	private array $companies;

	/**
	 * @var CompanyManager Company manager
	 */
	private CompanyManager $manager;

	/**
	 * Tests the function to list all companies
	 */
	public function testList(): void {
		$this->mockRequest('companies', 'companies/data');
		Assert::equal($this->companies, $this->manager->list());
	}

	/**
	 * Tests the function to get the company by its ID
	 */
	public function testGet(): void {
		$this->mockRequest('companies/2', 'companies/2');
		Assert::equal($this->companies[1], $this->manager->get(2));
	}

	/**
	 * Tests the function to get the company by its ID (company not found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('companies/9999', 'companies/2', null, new DataNotFound());
		Assert::throws(function (): void {
			$this->manager->get(9999);
		}, CompanyNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->companies = [
			new Company(
				id: 1,
				name: 'Protronix s.r.o.',
				homepage: 'https://www.protronix.cz',
			),
			new Company(
				id: 2,
				name: 'IQRF Tech s.r.o.',
				homepage: 'https://www.iqrf.org',
			),
		];
		$this->manager = new CompanyManager($this->apiClient);
	}

}

$test = new CompanyManagerTest();
$test->run();
