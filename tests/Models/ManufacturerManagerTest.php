<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Manufacturer;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ManufacturerNotFound;
use Iqrf\Repository\Models\ManufacturerManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for the manufacturer manager
 */
class ManufacturerManagerTest extends ApiTestCase {

	/**
	 * @var ManufacturerManager Manufacturer manager
	 */
	private ManufacturerManager $manager;

	/**
	 * @var array<Manufacturer> Manufacturers
	 */
	private array $manufacturers;

	/**
	 * Tests the function to list all manufacturers
	 */
	public function testList(): void {
		$this->mockRequest('manufacturers', 'manufacturers/data');
		Assert::equal($this->manufacturers, $this->manager->list());
	}

	/**
	 * Tests the function to get the manufacturer by its ID
	 */
	public function testGet(): void {
		$this->mockRequest('manufacturers/0', 'manufacturers/0');
		Assert::equal($this->manufacturers[1], $this->manager->get(0));
	}

	/**
	 * Tests the function to get the manufacturer by its ID (manufacturer not found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('manufacturers/9999', 'manufacturers/0', null, new DataNotFound());
		Assert::throws(function (): void {
			$this->manager->get(9999);
		}, ManufacturerNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new ManufacturerManager($this->apiClient);
		$this->manufacturers = [
			new Manufacturer(1, 1, 'Protronix s.r.o.'),
			new Manufacturer(0, 2, 'IQRF Tech s.r.o.'),
			new Manufacturer(2, 2, 'IQRF Tech s.r.o.'),
			new Manufacturer(94, 2, 'IQRF Tech s.r.o.'),
		];
	}

}

$test = new ManufacturerManagerTest();
$test->run();
