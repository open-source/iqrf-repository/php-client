<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Dpa;
use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\Os;
use Iqrf\Repository\Entities\OsDpa;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Enums\TransceiverFamily;
use Iqrf\Repository\Models\OsAndDpaManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF OS and DPA version manager
 */
class OsAndDpaManagerTest extends ApiTestCase {

	/**
	 * @var OsAndDpaManager IQRF OS and DPA version manager
	 */
	private OsAndDpaManager $manager;

	/**
	 * @var array<OsDpa> IQRF OS and DPA versions
	 */
	private array $versions;

	/**
	 * Tests the function to list all available IQRF OS and DPA versions
	 */
	public function testList(): void {
		$this->mockRequest('osdpa', 'osdpa/all', []);
		Assert::equal($this->versions, $this->manager->list());
	}

	/**
	 * Tests the function to list all available IQRF OS and DPA beta versions
	 */
	public function testListBeta(): void {
		$attributes = new OsDpaAttributes(true, null);
		$this->mockRequest('osdpa', 'osdpa/08D7_0416', ['dpaBeta' => 'true', 'osBeta' => 'true']);
		Assert::equal([$this->versions[2]], $this->manager->list($attributes));
	}

	/**
	 * Tests the function to get filtered IQRF OS and DPA versions
	 */
	public function testGet(): void {
		$this->mockRequest('osdpa', 'osdpa/08D5_0415', ['dpa' => '0415', 'os' => '08D5']);
		Assert::equal([$this->versions[0]], $this->manager->get('08D5', '0415'));
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new OsAndDpaManager($this->apiClient);
		$os = [
			'08D5' => new Os(
				build: '08D5',
				version: '4.04D',
				trFamily: TransceiverFamily::TR_7xD,
				attributes: OsDpaAttributes::fromInt(0),
				downloadPath: 'https://repository.iqrfalliance.org/download/iqrfos/08D5',
			),
			'08D7' => new Os(
				build: '08D7',
				version: '4.05D',
				trFamily: TransceiverFamily::TR_7xD,
				attributes: OsDpaAttributes::fromInt(1),
				downloadPath: 'https://repository.iqrfalliance.org/download/iqrfos/08D7',
			),
		];
		$dpa = [
			'0415' => new Dpa(
				version: new DpaVersion('0415'),
				attributes: OsDpaAttributes::fromInt(0),
				downloadPath: 'https://repository.iqrfalliance.org/download/dpa/4.15',
			),
			'0416' => new Dpa(
				version: new DpaVersion('0416'),
				attributes: OsDpaAttributes::fromInt(1),
				downloadPath: 'https://repository.iqrfalliance.org/download/dpa/4.16',
			),
		];
		$this->versions = [
			new OsDpa($os['08D5'], $dpa['0415'], 'IQRF OS 4.04D, DPA 4.15'),
			new OsDpa($os['08D5'], $dpa['0416'], 'IQRF OS 4.04D, DPA 4.16'),
			new OsDpa($os['08D7'], $dpa['0416'], 'IQRF OS 4.05D, DPA 4.16'),
		];
	}

}

$test = new OsAndDpaManagerTest();
$test->run();
