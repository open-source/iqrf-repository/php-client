<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use DateTime;
use Iqrf\Repository\Entities\ServerStatus;
use Iqrf\Repository\Models\ServerManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF Repository server manager
 */
class ServerManagerTest extends ApiTestCase {

	/**
	 * @var ServerManager IQRF Repository server manager
	 */
	private ServerManager $manager;

	/**
	 * Tests the function to get the server's status
	 */
	public function testGetStatus(): void {
		$this->mockRequest('server', 'server/data');
		$expected = new ServerStatus(
			apiVersion: 0,
			hostname: 'repository.iqrfalliance.org',
			user: 'guest',
			buildDateTime: new DateTime('2019-08-28T16:34:24+02:00'),
			startDateTime: new DateTime('2019-08-28T16:51:12+02:00'),
			dateTime: new DateTime('2019-09-11T14:14:51+02:00'),
			databaseChecksum: 5742889024523717677,
			databaseChangeDateTime: new DateTime('2019-08-28T14:01:30+02:00'),
		);
		Assert::equal($expected, $this->manager->getStatus());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new ServerManager($this->apiClient);
	}

}

$test = new ServerManagerTest();
$test->run();
