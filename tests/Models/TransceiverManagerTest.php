<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Transceiver;
use Iqrf\Repository\Entities\TransceiverComponents;
use Iqrf\Repository\Entities\TransceiverCurrent;
use Iqrf\Repository\Entities\TransceiverCurrentRf;
use Iqrf\Repository\Entities\TransceiverCurrentRfRx;
use Iqrf\Repository\Entities\TransceiverCurrentRfTx;
use Iqrf\Repository\Entities\TransceiverDimensions;
use Iqrf\Repository\Entities\TransceiverOperatingVoltage;
use Iqrf\Repository\Entities\TransceiverPins;
use Iqrf\Repository\Entities\TransceiverSeriesType;
use Iqrf\Repository\Entities\TransceiverSupplyVoltage;
use Iqrf\Repository\Entities\TransceiverTemperature;
use Iqrf\Repository\Entities\TransceiverVoltage;
use Iqrf\Repository\Entities\TrSeries;
use Iqrf\Repository\Enums\McuTypes;
use Iqrf\Repository\Enums\TransceiverAntenna;
use Iqrf\Repository\Enums\TransceiverMount;
use Iqrf\Repository\Enums\TransceiverStatus;
use Iqrf\Repository\Enums\TrDSeries;
use Iqrf\Repository\Enums\TrGSeries;
use Iqrf\Repository\Models\TransceiverManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver manager
 */
class TransceiverManagerTest extends ApiTestCase {

	/**
	 * @var array<Transceiver> Transceivers
	 */
	private array $transceivers;

	/**
	 * @var TransceiverManager Transceiver manager
	 */
	private TransceiverManager $manager;

	/**
	 * Tests function to list all transceivers
	 */
	public function testList(): void {
		$this->mockRequest('transceivers', 'transceivers/data');
		Assert::equal($this->transceivers, $this->manager->list());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->transceivers = [
			new Transceiver(
				homePage: 'https://www.iqrf.org/product-detail/tr-72d',
				status: TransceiverStatus::Standard,
				mounting: TransceiverMount::SIM,
				trSeries: new TrSeries(
					series: TrDSeries::TR_72D,
					imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
				),
				mcu: McuTypes::PIC16LF1938,
				components: new TransceiverComponents(
					rfChip: 'Spirit1',
					ldo: 'MCP1700',
					temperatureSensorChip: 'MCP9808',
				),
				pins: new TransceiverPins(
					total: 8,
					ioPins: 6,
					adInputs: 2,
				),
				voltage: new TransceiverVoltage(
					operating: new TransceiverOperatingVoltage(
						min: 2.9,
						max: 3.4,
						nominal: 3,
					),
					supply: new TransceiverSupplyVoltage(
						min: 3.1,
						max: 5.3,
					),
				),
				current: new TransceiverCurrent(
					rf: new TransceiverCurrentRf(
						tx: new TransceiverCurrentRfTx(
							min: 8300,
							max: 25000,
						),
						rx: new TransceiverCurrentRfRx(
							std: 12100,
							lp: 260,
							xlp: 18.5,
						),
						ready: 2800,
					),
					ledg: 2000,
					ledr: 2000,
					mcu: 1950,
					sleep: 2.3,
					deepSleep: 1.7,
				),
				operatingTemperature: new TransceiverTemperature(
					min: -40,
					max: 85,
				),
				types: [
					new TransceiverSeriesType(
						name: 'TR-72D',
						temperatureSensor: false,
						antenna: TransceiverAntenna::PAD,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 25.1,
						),
						imageUrl: null,
					),
					new TransceiverSeriesType(
						name: 'TR-72DA',
						temperatureSensor: false,
						antenna: TransceiverAntenna::PCB,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 31.8,
						),
						imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
					),
					new TransceiverSeriesType(
						name: 'TR-72DAT',
						temperatureSensor: true,
						antenna: TransceiverAntenna::PCB,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 31.8,
						),
						imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
					),
					new TransceiverSeriesType(
						name: 'TR-72DC',
						temperatureSensor: false,
						antenna: TransceiverAntenna::UFL,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 25.1,
						),
						imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DC.png',
					),
				],
			),
			new Transceiver(
				homePage: 'https://www.iqrf.org/product-detail/tr-76g',
				status: TransceiverStatus::New,
				mounting: TransceiverMount::SMT,
				trSeries: new TrSeries(
					series: TrGSeries::TR_76G,
					imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76GA_persp.png',
				),
				mcu: McuTypes::PIC16LF18877,
				components: new TransceiverComponents(
					rfChip: 'Spirit1',
					ldo: '',
					temperatureSensorChip: '',
				),
				pins: new TransceiverPins(
					total: 18,
					ioPins: 12,
					adInputs: 2,
				),
				voltage: new TransceiverVoltage(
					operating: new TransceiverOperatingVoltage(
						min: 2.9,
						max: 3.4,
						nominal: 3,
					),
					supply: new TransceiverSupplyVoltage(
						min: 3,
						max: 3.4,
					),
				),
				current: new TransceiverCurrent(
					rf: new TransceiverCurrentRf(
						tx: new TransceiverCurrentRfTx(
							min: 8000,
							max: 25000,
						),
						rx: new TransceiverCurrentRfRx(
							std: 12500,
							lp: 190,
							xlp: 13,
						),
						ready: 3300,
					),
					ledg: 0,
					ledr: 0,
					mcu: 1800,
					sleep: 1,
					deepSleep: 0.3,
				),
				operatingTemperature: new TransceiverTemperature(
					min: -40,
					max: 85,
				),
				types: [
					new TransceiverSeriesType(
						name: 'TR-76G',
						temperatureSensor: false,
						antenna: TransceiverAntenna::PAD,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 15.2,
						),
						imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76G.png',
					),
					new TransceiverSeriesType(
						name: 'TR-76GA',
						temperatureSensor: false,
						antenna: TransceiverAntenna::PCB,
						dimensions: new TransceiverDimensions(
							width: 14.9,
							height: 3.3,
							depth: 23.3,
						),
						imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76GA.png',
					),
				],
			),
		];
		$this->manager = new TransceiverManager($this->apiClient);
	}

}

$test = new TransceiverManagerTest();
$test->run();
