<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Quantity;
use Iqrf\Repository\Models\QuantityManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for quantity manager
 */
class QuantityManagerTest extends ApiTestCase {

	/**
	 * @var array<Quantity> Quantities
	 */
	private array $quantities;

	/**
	 * @var QuantityManager Quantity manager
	 */
	private QuantityManager $manager;

	/**
	 * Tests function to list all quantities
	 */
	public function testList(): void {
		$this->mockRequest('quantities', 'quantities/data');
		Assert::equal($this->quantities, $this->manager->list());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->quantities = [
			new Quantity(
				id: 'TEMPERATURE',
				name: 'Temperature',
				iqrfName: 'Temperature',
				shortName: 'T',
				unit: '°C',
				decimalPlaces: 4,
				frcs: [
					144,
					224,
				],
				helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x01-temperature.html',
				idValue: 1,
				width: 2,
				idDriver: 'iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE',
			),
			new Quantity(
				id: 'BINARYDATA7',
				name: 'Binary data7',
				iqrfName: 'Binary data7',
				shortName: 'bin7',
				unit: null,
				decimalPlaces: 0,
				frcs: [
					16,
					144,
				],
				helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x81-binary-data7.html',
				idValue: 129,
				width: 1,
				idDriver: 'iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7',
			),
			new Quantity(
				id: 'PH',
				name: 'pH',
				iqrfName: 'pH',
				shortName: 'pH',
				unit: null,
				decimalPlaces: 4,
				frcs: [
					144,
				],
				helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x84-ph.html',
				idValue: 132,
				width: 1,
				idDriver: 'iqrf.sensor.STD_SENSOR_TYPE_PH',
			),
		];
		$this->manager = new QuantityManager($this->apiClient);
	}

}

$test = new QuantityManagerTest();
$test->run();
