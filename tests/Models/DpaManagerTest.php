<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Dpa;
use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Exceptions\DpaNotFound;
use Iqrf\Repository\Models\DpaManager;
use Tester\Assert;
use Tests\Iqrf\Repository\TestCases\ApiTestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for DPA version manager
 */
class DpaManagerTest extends ApiTestCase {

	/**
	 * @var DpaManager DPA version manager
	 */
	private DpaManager $manager;

	/**
	 * @var array<Dpa> DPA versions
	 */
	private array $versions;

	/**
	 * Tests the function to list all available DPA versions
	 */
	public function testListAll(): void {
		$this->mockRequest('dpa', 'dpa/all', []);
		Assert::equal($this->versions, $this->manager->list());
	}

	/**
	 * Tests the function to list all available DPA beta versions
	 */
	public function testListBeta(): void {
		$attributes = new OsDpaAttributes(true, null);
		$this->mockRequest('dpa', 'dpa/0416', ['beta' => 'true']);
		Assert::equal([$this->versions[1]], $this->manager->list($attributes));
	}

	/**
	 * Tests the function to list all available DPA obsolete beta versions
	 */
	public function testListObsoleteBeta(): void {
		$attributes = new OsDpaAttributes(true, true);
		$this->mockRequest('dpa', 'emptyArray', ['beta' => 'true', 'obsolete' => 'true']);
		Assert::throws(function () use ($attributes): void {
			$this->manager->list($attributes);
		}, DpaNotFound::class);
	}

	/**
	 * Tests the function to list all available DPA stable versions
	 */
	public function testListStable(): void {
		$attributes = new OsDpaAttributes(false, false);
		$this->mockRequest('dpa', 'dpa/0415', ['beta' => 'false', 'obsolete' => 'false']);
		Assert::equal([$this->versions[0]], $this->manager->list($attributes));
	}

	/**
	 * Tests the function to get the DPA version
	 */
	public function testGet(): void {
		$this->mockRequest('dpa', 'dpa/0415', ['version' => '0415']);
		Assert::equal($this->versions[0], $this->manager->get('0415'));
	}

	/**
	 * Tests the function to get the DPA version (DPA version not found)
	 */
	public function testGetNotFound(): void {
		$this->mockRequest('dpa', 'emptyArray', ['version' => 'nonsence']);
		Assert::throws(function (): void {
			$this->manager->get('nonsence');
		}, DpaNotFound::class);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->manager = new DpaManager($this->apiClient);
		$this->versions = [
			new Dpa(
				version: new DpaVersion('0415'),
				attributes: OsDpaAttributes::fromInt(0),
				downloadPath: 'https://repository.iqrfalliance.org/download/dpa/4.15',
			),
			new Dpa(
				version: new DpaVersion('0416'),
				attributes: OsDpaAttributes::fromInt(1),
				downloadPath: 'https://repository.iqrfalliance.org/download/dpa/4.16',
			),
		];
	}

}

$test = new DpaManagerTest();
$test->run();
