<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\TestCases;

use Iqrf\Repository\Utils\ApiClient;
use Mockery;
use Mockery\MockInterface;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\TestCase;
use Throwable;

/**
 * JSON API test case
 */
abstract class ApiTestCase extends TestCase {

	/**
	 * @var ApiClient|MockInterface Mocked JSON API client
	 */
	protected ApiClient|MockInterface $apiClient;

	/**
	 * Mocks JSON API request
	 * @param string $path JSON API path
	 * @param string $fileName JSON response file name
	 * @param array<mixed>|null $query JSON API query
	 * @param Throwable|null $exception Exception to throw
	 */
	protected function mockRequest(string $path, string $fileName, ?array $query = null, ?Throwable $exception = null): void {
		$args = [$path];
		if ($query !== null) {
			$args[] = $query;
		}
		$response = Json::decode(FileSystem::read(__DIR__ . '/../data/' . $fileName . '.json'), forceArrays: true);
		$mock = $this->apiClient->shouldReceive('sendApiRequest')
			->withArgs($args)
			->andReturn($response);
		if ($exception === null) {
			return;
		}
		$mock->andThrowExceptions([$exception]);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->apiClient = Mockery::mock(ApiClient::class);
	}

	/**
	 * Cleanups the test environment
	 */
	protected function tearDown(): void {
		Mockery::close();
	}

}
