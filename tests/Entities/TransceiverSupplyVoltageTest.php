<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverSupplyVoltage;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver supply voltage entity
 */
class TransceiverSupplyVoltageTest extends TestCase {

	/**
	 * @var array{
	 *     supplyVoltageMin: float,
	 *     supplyVoltageMax: float,
	 * } API response
	 */
	private array $array = [
		'supplyVoltageMin' => 3.1,
		'supplyVoltageMax' => 5.3,
	];

	/**
	 * @var TransceiverSupplyVoltage Transceiver supply voltage entity
	 */
	private TransceiverSupplyVoltage $entity;

	/**
	 * Tests the function to get minimum supply voltage
	 */
	public function testGetMinimumSupplyVoltage(): void {
		Assert::equal($this->array['supplyVoltageMin'], $this->entity->min);
	}

	/**
	 * Tests the function to get maximum supply voltage
	 */
	public function testGetMaximumSupplyVoltage(): void {
		Assert::equal($this->array['supplyVoltageMax'], $this->entity->max);
	}

	/**
	 * Tests the function to create a new TransceiverSupplyVoltage entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverSupplyVoltage::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverSupplyVoltage entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'min' => 3.1,
			'max' => 5.3,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverSupplyVoltage(
			min: $this->array['supplyVoltageMin'],
			max: $this->array['supplyVoltageMax'],
		);
		parent::setUp();
	}

}

$test = new TransceiverSupplyVoltageTest();
$test->run();
