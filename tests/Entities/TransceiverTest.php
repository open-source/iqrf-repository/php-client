<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Transceiver;
use Iqrf\Repository\Entities\TransceiverComponents;
use Iqrf\Repository\Entities\TransceiverCurrent;
use Iqrf\Repository\Entities\TransceiverCurrentRf;
use Iqrf\Repository\Entities\TransceiverCurrentRfRx;
use Iqrf\Repository\Entities\TransceiverCurrentRfTx;
use Iqrf\Repository\Entities\TransceiverDimensions;
use Iqrf\Repository\Entities\TransceiverOperatingVoltage;
use Iqrf\Repository\Entities\TransceiverPins;
use Iqrf\Repository\Entities\TransceiverSeriesType;
use Iqrf\Repository\Entities\TransceiverSupplyVoltage;
use Iqrf\Repository\Entities\TransceiverTemperature;
use Iqrf\Repository\Entities\TransceiverVoltage;
use Iqrf\Repository\Entities\TrSeries;
use Iqrf\Repository\Enums\McuTypes;
use Iqrf\Repository\Enums\TransceiverAntenna;
use Iqrf\Repository\Enums\TransceiverMount;
use Iqrf\Repository\Enums\TransceiverStatus;
use Iqrf\Repository\Enums\TrDSeries;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver entity
 */
class TransceiverTest extends TestCase {

	/**
	 * @var array{
	 *     seriesName: string,
	 *     homePage: string,
	 *     status: string,
	 *     mounting: string,
	 *     seriesMIcode: int,
	 *     mcuMIcode: int,
	 *     mcu: string,
	 *     rfChip: string,
	 *     ldo: string,
	 *     temperatureSensorChip: string,
	 *     pins: int,
	 *     ioPins: int,
	 *     adInputs: int,
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 *     supplyVoltageMin: float,
	 *     supplyVoltageMax: float,
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 *     currentRfReady: float,
	 *     currentLEDG: float,
	 *     currentLEDR: float,
	 *     currentMcu: float,
	 *     currentSleep: float,
	 *     currentDeepSleep: float,
	 *     operatingTemperatureMin: float,
	 *     operatingTemperatureMax: float,
	 *     seriesImage: string,
	 *     types: array<array{
	 *         typeName: string,
	 *         temperatureSensor: bool,
	 *         antenna: string,
	 *         dimensions: string,
	 *         image?: string,
	 *     }>
	 * } API response
	 */
	private array $array = [
		'seriesName' => 'TR-72D',
		'homePage' => 'https://www.iqrf.org/product-detail/tr-72d',
		'status' => 'Standard',
		'mounting' => 'SIM',
		'mcuMIcode' => 4,
		'mcu' => 'PIC16LF1938',
		'seriesMIcode' => 2,
		'rfChip' => 'Spirit1',
		'ldo' => 'MCP1700',
		'temperatureSensorChip' => 'MCP9808',
		'pins' => 8,
		'ioPins' => 6,
		'adInputs' => 2,
		'operatingVoltageMin' => 2.9,
		'operatingVoltageMax' => 3.4,
		'operatingVoltageNominal' => 3.0,
		'supplyVoltageMin' => 3.1,
		'supplyVoltageMax' => 5.3,
		'currentRfTxMin' => 8300.0,
		'currentRfTxMax' => 25000.0,
		'currentRfRxStd' => 12100.0,
		'currentRfRxLp' => 260.0,
		'currentRfRxXlp' => 18.5,
		'currentRfReady' => 2800.0,
		'currentLEDG' => 2000.0,
		'currentLEDR' => 2000.0,
		'currentMcu' => 1950.0,
		'currentSleep' => 2.3,
		'currentDeepSleep' => 1.7,
		'operatingTemperatureMin' => -40.0,
		'operatingTemperatureMax' => 85.0,
		'seriesImage' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
		'types' => [
			[
				'typeName' => 'TR-72D',
				'temperatureSensor' => false,
				'antenna' => 'Pad',
				'dimensions' => '14.9×3.3×25.1',
			],
			[
				'typeName' => 'TR-72DA',
				'temperatureSensor' => false,
				'antenna' => 'PCB',
				'dimensions' => '14.9×3.3×31.8',
				'image' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
			],
			[
				'typeName' => 'TR-72DAT',
				'temperatureSensor' => true,
				'antenna' => 'PCB',
				'dimensions' => '14.9×3.3×31.8',
				'image' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',

			],
			[
				'typeName' => 'TR-72DC',
				'temperatureSensor' => false,
				'antenna' => 'U.FL',
				'dimensions' => '14.9×3.3×25.1',
				'image' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DC.png',
			],
		],
	];

	/**
	 * @var Transceiver Transceiver entity
	 */
	private Transceiver $entity;

	/**
	 * Tests the function to get transceiver homepage
	 */
	public function getHomePage(): void {
		Assert::same($this->array['homePage'], $this->entity->homePage);
	}

	/**
	 * Tests the function to get transceiver status
	 */
	public function getStatus(): void {
		$expected = TransceiverStatus::from($this->array['status']);
		Assert::same($expected, $this->entity->status);
	}

	/**
	 * Tests the function to get transceiver mounting
	 */
	public function getMounting(): void {
		$expected = TransceiverMount::from($this->array['mounting']);
		Assert::same($expected, $this->entity->mounting);
	}

	/**
	 * Tests the function to get transceiver series code
	 */
	public function testGetSeriesCode(): void {
		$expected = TrDSeries::from($this->array['seriesMIcode']);
		Assert::same($expected, $this->entity->trSeries->series);
	}

	/**
	 * Tests the function to get transceiver series name
	 */
	public function testGetSeriesName(): void {
		Assert::same($this->array['seriesName'], $this->entity->trSeries->series->toString());
	}

	/**
	 * Tests the function to get transceiver series image
	 */
	public function testGetSeriesImage(): void {
		Assert::same($this->array['seriesImage'], $this->entity->trSeries->imageUrl);
	}

	/**
	 * Tests the function to get transceiver mcu code
	 */
	public function testGetMcuCode(): void {
		$expected = McuTypes::from($this->array['mcuMIcode']);
		Assert::same($expected, $this->entity->mcu);
	}

	/**
	 * Tests the function to get transceiver mcu name
	 */
	public function testGetMcuName(): void {
		Assert::same($this->array['mcu'], $this->entity->mcu->toString());
	}

	/**
	 * Tests the function to get transceiver rf chip
	 */
	public function testGetRfChip(): void {
		Assert::same($this->array['rfChip'], $this->entity->components->rfChip);
	}

	/**
	 * Tests the function to get transceiver ldo chip
	 */
	public function testGetLdo(): void {
		Assert::same($this->array['ldo'], $this->entity->components->ldo);
	}

	/**
	 * Tests the function to get transceiver temperature sensor chip
	 */
	public function testGetTemperatureSensorChip(): void {
		Assert::same($this->array['temperatureSensorChip'], $this->entity->components->temperatureSensorChip);
	}

	/**
	 * Tests the function to get transceiver total pins
	 */
	public function testGetTotalPins(): void {
		Assert::same($this->array['pins'], $this->entity->pins->total);
	}

	/**
	 * Tests the function to get transceiver io pins
	 */
	public function testGetIoPins(): void {
		Assert::same($this->array['ioPins'], $this->entity->pins->ioPins);
	}

	/**
	 * Tests the function to get transceiver ad inputs
	 */
	public function testGetAdInputs(): void {
		Assert::same($this->array['adInputs'], $this->entity->pins->adInputs);
	}

	/**
	 * Tests the function to get minimum operating voltage
	 */
	public function testGetMinimumOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageMin'], $this->entity->voltage->operating->min);
	}

	/**
	 * Tests the function to get maximum operating voltage
	 */
	public function testGetMaximumOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageMax'], $this->entity->voltage->operating->max);
	}

	/**
	 * Tests the function to get nominal operating voltage
	 */
	public function testGetNominalOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageNominal'], $this->entity->voltage->operating->nominal);
	}

	/**
	 * Tests the function to get minimum supply voltage
	 */
	public function testGetMinimumSupplyVoltage(): void {
		Assert::same($this->array['supplyVoltageMin'], $this->entity->voltage->supply->min);
	}

	/**
	 * Tests the function to get maximum supply voltage
	 */
	public function testGetMaximumSupplyVoltage(): void {
		Assert::same($this->array['supplyVoltageMax'], $this->entity->voltage->supply->max);
	}

	/**
	 * Tests the function to get minimum RF TX current
	 */
	public function testGetTxMinCurrent(): void {
		Assert::equal($this->array['currentRfTxMin'], $this->entity->current->rf->tx->min);
	}

	/**
	 * Tests the function to get maximum RF TX current
	 */
	public function testGetTxMaxCurrent(): void {
		Assert::equal($this->array['currentRfTxMax'], $this->entity->current->rf->tx->max);
	}

	/**
	 * Tests the function to get RF RX STD current
	 */
	public function testGetRxStdCurrent(): void {
		Assert::equal($this->array['currentRfRxStd'], $this->entity->current->rf->rx->std);
	}

	/**
	 * Tests the function to get RF RX LP current
	 */
	public function testGetRxLpCurrent(): void {
		Assert::equal($this->array['currentRfRxLp'], $this->entity->current->rf->rx->lp);
	}

	/**
	 * Tests the function to get RF RX XLP current
	 */
	public function testGetRxXlpCurrent(): void {
		Assert::equal($this->array['currentRfRxXlp'], $this->entity->current->rf->rx->xlp);
	}

	/**
	 * Tests the function to get RF ready current
	 */
	public function testGetReadyCurrent(): void {
		Assert::equal($this->array['currentRfReady'], $this->entity->current->rf->ready);
	}

	/**
	 * Tests the function to get LEDG current
	 */
	public function testGetLedgCurrent(): void {
		Assert::equal($this->array['currentLEDG'], $this->entity->current->ledg);
	}

	/**
	 * Tests the function to get LEDR current
	 */
	public function testGetLedrCurrent(): void {
		Assert::equal($this->array['currentLEDR'], $this->entity->current->ledr);
	}

	/**
	 * Tests the function to get MCU current
	 */
	public function testGetMcuCurrent(): void {
		Assert::equal($this->array['currentMcu'], $this->entity->current->mcu);
	}

	/**
	 * Tests the function to get sleep current
	 */
	public function testGetSleepCurrent(): void {
		Assert::equal($this->array['currentSleep'], $this->entity->current->sleep);
	}

	/**
	 * Tests the function to get deep sleep current
	 */
	public function testGetDeepSleepCurrent(): void {
		Assert::equal($this->array['currentDeepSleep'], $this->entity->current->deepSleep);
	}

	/**
	 * Tests the function to get minimum operating temperature
	 */
	public function testGetMinimumOperatingTemperature(): void {
		Assert::equal($this->array['operatingTemperatureMin'], $this->entity->operatingTemperature->min);
	}

	/**
	 * Tests the function to get maximum operating temperature
	 */
	public function testGetMaximumOperatingTemperature(): void {
		Assert::equal($this->array['operatingTemperatureMax'], $this->entity->operatingTemperature->max);
	}

	/**
	 * Tests the function to get transceiver type name
	 */
	public function testGetName(): void {
		Assert::same($this->array['types'][1]['typeName'], $this->entity->types[1]->name);
	}

	/**
	 * Tests the function to get transceiver type temperature sensor present
	 */
	public function testGetTemperatureSensor(): void {
		Assert::same($this->array['types'][1]['temperatureSensor'], $this->entity->types[1]->temperatureSensor);
	}

	/**
	 * Tests the function to get transceiver type antenna
	 */
	public function testGetAntenna(): void {
		$expected = TransceiverAntenna::from($this->array['types'][1]['antenna']);
		Assert::same($expected, $this->entity->types[1]->antenna);
	}

	/**
	 * Tests the function to get transceiver type width
	 */
	public function testGetWidth(): void {
		$expected = floatval(explode('×', $this->array['types'][1]['dimensions'], 3)[0]);
		Assert::equal($expected, $this->entity->types[1]->dimensions->width);
	}

	/**
	 * Tests the function to get transceiver type height
	 */
	public function testGetHeight(): void {
		$expected = floatval(explode('×', $this->array['types'][1]['dimensions'], 3)[1]);
		Assert::same($expected, $this->entity->types[1]->dimensions->height);
	}

	/**
	 * Tests the function to get transceiver type depth
	 */
	public function testGetDepth(): void {
		$expected = floatval(explode('×', $this->array['types'][1]['dimensions'], 3)[2]);
		Assert::same($expected, $this->entity->types[1]->dimensions->depth);
	}

	/**
	 * Tests the function to get transceiver type image
	 */
	public function testGetImage(): void {
		Assert::same($this->array['types'][1]['image'] ?? null, $this->entity->types[1]->imageUrl);
	}

	/**
	 * Tests the function to create a new Transceiver entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Transceiver::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize Transceiver entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'homePage' => 'https://www.iqrf.org/product-detail/tr-72d',
			'status' => 'Standard',
			'mounting' => 'SIM',
			'trFamily' => '7xD',
			'trSeries' => [
				'value' => 2,
				'name' => 'TR-72D',
				'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
			],
			'mcu' => [
				'value' => 4,
				'name' => 'PIC16LF1938',
			],
			'components' => [
				'rfChip' => 'Spirit1',
				'ldo' => 'MCP1700',
				'temperatureSensorChip' => 'MCP9808',
			],
			'pins' => [
				'total' => 8,
				'ioPins' => 6,
				'adInputs' => 2,
			],
			'voltage' => [
				'operating' => [
					'min' => 2.9,
					'max' => 3.4,
					'nominal' => 3.0,
				],
				'supply' => [
					'min' => 3.1,
					'max' => 5.3,
				],
			],
			'current' => [
				'rf' => [
					'tx' => [
						'min' => 8300.0,
						'max' => 25000.0,
					],
					'rx' => [
						'std' => 12100.0,
						'lp' => 260.0,
						'xlp' => 18.5,
					],
					'ready' => 2800.0,
				],
				'ledg' => 2000.0,
				'ledr' => 2000.0,
				'mcu' => 1950.0,
				'sleep' => 2.3,
				'deepSleep' => 1.7,
			],
			'operatingTemperature' => [
				'min' => -40.0,
				'max' => 85.0,
			],
			'types' => [
				[
					'name' => 'TR-72D',
					'temperatureSensor' => false,
					'antenna' => 'Pad',
					'dimensions' => [
						'width' => 14.9,
						'height' => 3.3,
						'depth' => 25.1,
					],
					'imageUrl' => null,
				],
				[
					'name' => 'TR-72DA',
					'temperatureSensor' => false,
					'antenna' => 'PCB',
					'dimensions' => [
						'width' => 14.9,
						'height' => 3.3,
						'depth' => 31.8,
					],
					'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
				],
				[
					'name' => 'TR-72DAT',
					'temperatureSensor' => true,
					'antenna' => 'PCB',
					'dimensions' => [
						'width' => 14.9,
						'height' => 3.3,
						'depth' => 31.8,
					],
					'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
				],
				[
					'name' => 'TR-72DC',
					'temperatureSensor' => false,
					'antenna' => 'U.FL',
					'dimensions' => [
						'width' => 14.9,
						'height' => 3.3,
						'depth' => 25.1,
					],
					'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DC.png',
				],
			],
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Transceiver(
			homePage: $this->array['homePage'],
			status: TransceiverStatus::from($this->array['status']),
			mounting: TransceiverMount::from($this->array['mounting']),
			trSeries: new TrSeries(
				series: TrDSeries::TR_72D,
				imageUrl: $this->array['seriesImage'],
			),
			mcu: McuTypes::from($this->array['mcuMIcode']),
			components: new TransceiverComponents(
				rfChip: $this->array['rfChip'],
				ldo: $this->array['ldo'],
				temperatureSensorChip: $this->array['temperatureSensorChip'],
			),
			pins: new TransceiverPins(
				total: $this->array['pins'],
				ioPins: $this->array['ioPins'],
				adInputs: $this->array['adInputs'],
			),
			voltage: new TransceiverVoltage(
				operating: new TransceiverOperatingVoltage(
					min: $this->array['operatingVoltageMin'],
					max: $this->array['operatingVoltageMax'],
					nominal: $this->array['operatingVoltageNominal'],
				),
				supply: new TransceiverSupplyVoltage(
					min: $this->array['supplyVoltageMin'],
					max: $this->array['supplyVoltageMax'],
				),
			),
			current: new TransceiverCurrent(
				rf: new TransceiverCurrentRf(
					tx: new TransceiverCurrentRfTx(
						min: $this->array['currentRfTxMin'],
						max: $this->array['currentRfTxMax'],
					),
					rx: new TransceiverCurrentRfRx(
						std: $this->array['currentRfRxStd'],
						lp: $this->array['currentRfRxLp'],
						xlp: $this->array['currentRfRxXlp'],
					),
					ready: $this->array['currentRfReady'],
				),
				ledg: $this->array['currentLEDG'],
				ledr: $this->array['currentLEDR'],
				mcu: $this->array['currentMcu'],
				sleep: $this->array['currentSleep'],
				deepSleep: $this->array['currentDeepSleep'],
			),
			operatingTemperature: new TransceiverTemperature(
				min: $this->array['operatingTemperatureMin'],
				max: $this->array['operatingTemperatureMax'],
			),
			types: array_map(static fn (array $type): TransceiverSeriesType => new TransceiverSeriesType(
				name: $type['typeName'],
				temperatureSensor: $type['temperatureSensor'],
				antenna: TransceiverAntenna::from($type['antenna']),
				dimensions: new TransceiverDimensions(
					...array_map(static fn (string $token): float => floatval($token), explode('×', $type['dimensions'], 3))
				),
				imageUrl: $type['image'] ?? null,
			), $this->array['types']),
		);
		parent::setUp();
	}

}

$test = new TransceiverTest();
$test->run();
