<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverTemperature;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver operating temperature entity
 */
class TransceiverTemperatureTest extends TestCase {

	/**
	 * @var array{
	 *     operatingTemperatureMin: float,
	 *     operatingTemperatureMax: float,
	 * } API response
	 */
	private array $array = [
		'operatingTemperatureMin' => -40.0,
		'operatingTemperatureMax' => 85.0,
	];

	/**
	 * @var TransceiverTemperature Transceiver operating temperature entity
	 */
	private TransceiverTemperature $entity;

	/**
	 * Tests the function to get minimum operating temperature
	 */
	public function testGetMinimumOperatingTemperature(): void {
		Assert::equal($this->array['operatingTemperatureMin'], $this->entity->min);
	}

	/**
	 * Tests the function to get maximum operating temperature
	 */
	public function testGetMaximumOperatingTemperature(): void {
		Assert::equal($this->array['operatingTemperatureMax'], $this->entity->max);
	}

	/**
	 * Tests the function to create a new TransceiverTemperature entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverTemperature::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverTemperature entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'min' => -40.0,
			'max' => 85.0,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverTemperature(
			min: $this->array['operatingTemperatureMin'],
			max: $this->array['operatingTemperatureMax'],
		);
		parent::setUp();
	}

}

$test = new TransceiverTemperatureTest();
$test->run();
