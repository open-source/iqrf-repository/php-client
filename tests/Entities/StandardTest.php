<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Standard;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF Standard entity
 */
class StandardTest extends TestCase {

	/**
	 * @var array{
	 *     standardID: int,
	 *     name: string,
	 * } API response
	 */
	private array $array = [
		'standardID' => 0,
		'name' => 'Embed: Coordinator',
	];

	/**
	 * @var Standard IQRF Standard entity
	 */
	private Standard $entity;

	/**
	 * Tests the function to get the IQRF Standard ID
	 */
	public function testGetId(): void {
		Assert::same($this->array['standardID'], $this->entity->id);
	}

	/**
	 * Tests the function to get the IQRF Standard name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to create a new IQRF Standard entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Standard::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the IQRF Standard entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 0,
			'name' => 'Embed: Coordinator',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Standard(
			id: $this->array['standardID'],
			name: $this->array['name'],
		);
		parent::setUp();
	}

}

$test = new StandardTest();
$test->run();
