<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Dpa;
use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\Os;
use Iqrf\Repository\Entities\OsDpa;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Enums\TransceiverFamily;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF OS and DPA entity
 */
class OsDpaTest extends TestCase {

	/**
	 * @var array{
	 *     os: string,
	 *     osVersion: string,
	 *     osTrFamily: string,
	 *     osAttributes: int,
	 *     osDownloadPath: string,
	 *     dpa: string,
	 *     dpaAttributes: int,
	 *     dpaDownloadPath: string,
	 *     notes: string,
	 * } API response
	 */
	private array $array;

	/**
	 * @var OsDpa IQRF OS and DPA entity
	 */
	private OsDpa $entity;

	/**
	 * @var Dpa DPA version entity
	 */
	private Dpa $dpa;

	/**
	 * @var Os IQRF OS version entity
	 */
	private Os $os;

	/**
	 * @var TransceiverFamily Transceiver family
	 */
	private TransceiverFamily $trFamily = TransceiverFamily::TR_7xD;

	/**
	 * Tests the function to get the IQRF OS version entity
	 */
	public function testGetOs(): void {
		Assert::equal($this->os, $this->entity->os);
	}

	/**
	 * Tests the function to get the DPA version entity
	 */
	public function testGetDpa(): void {
		Assert::equal($this->dpa, $this->entity->dpa);
	}

	/**
	 * Tests the function to get the notes
	 */
	public function testGetNotes(): void {
		Assert::same($this->array['notes'], $this->entity->notes);
	}

	/**
	 * Tests the function to create a new IQRF OS and DPA entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, OsDpa::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize IQRF OS and DPA entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'os' => [
				'build' => '08D5',
				'version' => '4.04D',
				'trFamily' => 'TR-7xD',
				'attributes' => ['beta' => false, 'obsolete' => false],
				'downloadPath' => 'https://repository.iqrfalliance.org/download/iqrfos/08D5',
			],
			'dpa' => [
				'version' => '4.15',
				'attributes' => ['beta' => false, 'obsolete' => false],
				'downloadPath' => 'https://repository.iqrfalliance.org/download/dpa/4.15',
			],
			'notes' => 'IQRF OS 4.04D, DPA 4.15',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->array = Json::decode(FileSystem::read(__DIR__ . '/../data/osdpa/08D5_0415.json'), forceArrays: true)[0];
		$this->os = new Os(
			build: $this->array['os'],
			version: $this->array['osVersion'],
			trFamily: $this->trFamily,
			attributes: OsDpaAttributes::fromInt($this->array['osAttributes']),
			downloadPath: $this->array['osDownloadPath'],
		);
		$this->dpa = new Dpa(
			version: new DpaVersion($this->array['dpa']),
			attributes: OsDpaAttributes::fromInt($this->array['dpaAttributes']),
			downloadPath: $this->array['dpaDownloadPath'],
		);
		$this->entity = new OsDpa(
			os: $this->os,
			dpa: $this->dpa,
			notes: $this->array['notes'],
		);
		parent::setUp();
	}

}

$test = new OsDpaTest();
$test->run();
