<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Product;
use Iqrf\Repository\Enums\RfModes;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for product entity
 */
class ProductTest extends TestCase {

	/**
	 * @var array{
	 *     hwpid: int,
	 *     name: string,
	 *     manufacturerID: int,
	 *     companyName: string,
	 *     homePage: string,
	 *     picture: string,
	 *     rfMode: int,
	 * } API response
	 */
	private array $array = [
		'hwpid' => 2,
		'name' => 'DDC-SE-01 sensor example',
		'manufacturerID' => 2,
		'companyName' => 'IQRF Tech s.r.o.',
		'homePage' => 'https://www.iqrf.org/products/development-tools/development-kits/ddc-se-01',
		'picture' => 'https://repository.iqrfalliance.org/productpictures/0002.png',
		'rfMode' => 1,
	];

	/**
	 * @var Product Product entity
	 */
	private Product $entity;

	/**
	 * Tests the function to get the product HWPID
	 */
	public function testGetHwpid(): void {
		Assert::same($this->array['hwpid'], $this->entity->hwpid);
	}

	/**
	 * Tests the function to get the product name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to get the manufacturer ID
	 */
	public function testGetManufacturerId(): void {
		Assert::same($this->array['manufacturerID'], $this->entity->manufacturerId);
	}

	/**
	 * Tests the function to get the company name
	 */
	public function testGetCompanyId(): void {
		Assert::same($this->array['companyName'], $this->entity->companyName);
	}

	/**
	 * Tests the function to get the product homepage
	 */
	public function testGetHomepage(): void {
		Assert::same($this->array['homePage'], $this->entity->homepage);
	}

	/**
	 * Tests the function to get the product picture
	 */
	public function testGetPicture(): void {
		Assert::same($this->array['picture'], $this->entity->picture);
	}

	/**
	 * Tests the function to get the product RF mode
	 */
	public function testGetRfMode(): void {
		$expected = RfModes::from($this->array['rfMode']);
		Assert::same($expected, $this->entity->rfMode);
	}

	/**
	 * Tests the function to create a new manufacturer entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Product::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the Product entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'hwpid' => 2,
			'name' => 'DDC-SE-01 sensor example',
			'manufacturerId' => 2,
			'companyName' => 'IQRF Tech s.r.o.',
			'homepage' => 'https://www.iqrf.org/products/development-tools/development-kits/ddc-se-01',
			'picture' => 'https://repository.iqrfalliance.org/productpictures/0002.png',
			'rfMode' => 'STD',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Product(
			hwpid: $this->array['hwpid'],
			name: $this->array['name'],
			manufacturerId: $this->array['manufacturerID'],
			companyName: $this->array['companyName'],
			homepage: $this->array['homePage'],
			picture: $this->array['picture'],
			rfMode: RfModes::STD,
		);
		parent::setUp();
	}

}

$test = new ProductTest();
$test->run();
