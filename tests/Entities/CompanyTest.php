<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Company;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for company entity
 */
class CompanyTest extends TestCase {

	/**
	 * @var array{
	 *     companyID: int,
	 *     name: string,
	 *     homePage: string,
	 * } API response
	 */
	private array $array = [
		'companyID' => 2,
		'name' => 'IQRF Tech s.r.o.',
		'homePage' => 'https://www.iqrf.org',
	];

	/**
	 * @var Company Company entity
	 */
	private Company $entity;

	/**
	 * Tests the function to create a new company entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Company::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the Company entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 2,
			'name' => 'IQRF Tech s.r.o.',
			'homepage' => 'https://www.iqrf.org',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Company(
			id: $this->array['companyID'],
			name: $this->array['name'],
			homepage: $this->array['homePage'],
		);
		parent::setUp();
	}

}

$test = new CompanyTest();
$test->run();
