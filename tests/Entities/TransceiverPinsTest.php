<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverPins;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver pins entity
 */
class TransceiverPinsTest extends TestCase {

	/**
	 * @var array{
	 *     pins: int,
	 *     ioPins: int,
	 *     adInputs: int,
	 * } API response
	 */
	private array $array = [
		'pins' => 8,
		'ioPins' => 6,
		'adInputs' => 2,
	];

	/**
	 * @var TransceiverPins Transceiver pins entity
	 */
	private TransceiverPins $entity;

	/**
	 * Tests the function to get total pins
	 */
	public function testGetTotalPins(): void {
		Assert::same($this->array['pins'], $this->entity->total);
	}

	/**
	 * Tests the function to get IO pins
	 */
	public function testGetIoPins(): void {
		Assert::same($this->array['ioPins'], $this->entity->ioPins);
	}

	/**
	 * Tests the function to get AD inputs
	 */
	public function testGetAdInputs(): void {
		Assert::same($this->array['adInputs'], $this->entity->adInputs);
	}

	/**
	 * Tests the function to create a new TransceiverPins entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverPins::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverPins entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'total' => 8,
			'ioPins' => 6,
			'adInputs' => 2,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverPins(
			total: $this->array['pins'],
			ioPins: $this->array['ioPins'],
			adInputs: $this->array['adInputs'],
		);
		parent::setUp();
	}

}

$test = new TransceiverPinsTest();
$test->run();
