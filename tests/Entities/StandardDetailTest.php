<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\StandardDetail;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for detailed IQRF Standard entity
 */
class StandardDetailTest extends TestCase {

	/**
	 * @var array{
	 *     standardID: int,
	 *     name: string,
	 *     version: float,
	 *     versionFlags: int,
	 *     driver: string,
	 *     notes: string,
	 * } API response
	 */
	private array $array = [
		'standardID' => 0,
		'name' => 'Embed: Coordinator',
		'version' => 2.0,
		'versionFlags' => 1,
		'driver' => 'Some JS code...',
		'notes' => 'Some notes...',
	];

	/**
	 * @var StandardDetail Detailed IQRF Standard entity
	 */
	private StandardDetail $entity;

	/**
	 * Tests the function to get the IQRF Standard ID
	 */
	public function testGetId(): void {
		Assert::same($this->array['standardID'], $this->entity->id);
	}

	/**
	 * Tests the function to get the IQRF Standard name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to get the IQRF Standard version
	 */
	public function testGetVersion(): void {
		Assert::same($this->array['version'], $this->entity->version);
	}

	/**
	 * Tests the function to get the IQRF Standard version flags
	 */
	public function testGetVersionFlags(): void {
		Assert::same($this->array['versionFlags'], $this->entity->versionFlags);
	}

	/**
	 * Tests the function to get the IQRF Standard driver code
	 */
	public function testGetDriver(): void {
		Assert::same($this->array['driver'], $this->entity->driver);
	}

	/**
	 * Tests the function to get the IQRF Standard notes
	 */
	public function testGetNotes(): void {
		Assert::same($this->array['notes'], $this->entity->notes);
	}

	/**
	 * Tests the function to create a new detailed IQRF Standard entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, StandardDetail::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the detailed IQRF Standard entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 0,
			'name' => 'Embed: Coordinator',
			'version' => 2.0,
			'versionFlags' => 1,
			'driver' => 'Some JS code...',
			'notes' => 'Some notes...',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new StandardDetail(
			id: $this->array['standardID'],
			name: $this->array['name'],
			version: $this->array['version'],
			versionFlags: $this->array['versionFlags'],
			driver: $this->array['driver'],
			notes: $this->array['notes'],
		);
		parent::setUp();
	}

}

$test = new StandardDetailTest();
$test->run();
