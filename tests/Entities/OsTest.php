<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Os;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Enums\TransceiverFamily;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF OS entity
 */
class OsTest extends TestCase {

	/**
	 * @var array{
	 *     build: string,
	 *     version: string,
	 *     trFamily: string,
	 *     attributes: int,
	 *     downloadPath: string,
	 * } API response
	 */
	private array $array;

	/**
	 * @var OsDpaAttributes IQRF OS version attributes
	 */
	private OsDpaAttributes $attributes;

	/**
	 * @var TransceiverFamily Transceiver family
	 */
	private TransceiverFamily $trFamily = TransceiverFamily::TR_7xD;

	/**
	 * @var Os IQRF OS version entity
	 */
	private Os $entity;

	/**
	 * Tests the function to get the IQRF OS build
	 */
	public function testGetBuild(): void {
		Assert::same($this->array['build'], $this->entity->build);
	}

	/**
	 * Tests the function to get the IQRF OS version
	 */
	public function testGetVersion(): void {
		Assert::same($this->array['version'], $this->entity->version);
	}

	/**
	 * Tests the function to get the transceiver family
	 */
	public function testGetTrFamily(): void {
		Assert::same($this->trFamily, $this->entity->trFamily);
	}

	/**
	 * Tests the function to get the version attributes
	 */
	public function testGetAttributes(): void {
		Assert::same($this->attributes, $this->entity->attributes);
	}

	/**
	 * Tests the function to get the downloadPath
	 */
	public function testGetDownloadPath(): void {
		Assert::same($this->array['downloadPath'], $this->entity->downloadPath);
	}

	/**
	 * Tests the function to create a new IQRF OS entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Os::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize IQRF OS entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'build' => '08D5',
			'version' => '4.04D',
			'trFamily' => 'TR-7xD',
			'attributes' => [
				'beta' => false,
				'obsolete' => false,
			],
			'downloadPath' => 'https://repository.iqrfalliance.org/download/iqrfos/08D5',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->array = Json::decode(FileSystem::read(__DIR__ . '/../data/os/08D5.json'), forceArrays: true)[0];
		$this->attributes = new OsDpaAttributes(false, false);
		$this->entity = new Os(
			build: $this->array['build'],
			version: $this->array['version'],
			trFamily: $this->trFamily,
			attributes: $this->attributes,
			downloadPath: $this->array['downloadPath'],
		);
		parent::setUp();
	}

}

$test = new OsTest();
$test->run();
