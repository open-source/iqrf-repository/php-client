<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\StandardVersions;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for IQRF Standard versions entity
 */
class StandardVersionsTest extends TestCase {

	/**
	 * @var array{
	 *     standardID: int,
	 *     name: string,
	 *     versions: array<float>,
	 * } API response
	 */
	private array $array = [
		'standardID' => 0,
		'name' => 'Embed: Coordinator',
		'versions' => [0, 1, 2],
	];

	/**
	 * @var StandardVersions IQRF Standard with versions entity
	 */
	private StandardVersions $entity;

	/**
	 * Tests the function to get the IQRF Standard ID
	 */
	public function testGetId(): void {
		Assert::same($this->array['standardID'], $this->entity->id);
	}

	/**
	 * Tests the function to get the IQRF Standard name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to get the IQRF Standard versions
	 */
	public function testGetVersions(): void {
		Assert::same($this->array['versions'], $this->entity->versions);
	}

	/**
	 * Tests the function to create a new IQRF Standard with versions entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, StandardVersions::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize IQRF Stansard versions entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 0,
			'name' => 'Embed: Coordinator',
			'versions' => [0, 1, 2],
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new StandardVersions(
			id: $this->array['standardID'],
			name: $this->array['name'],
			versions: $this->array['versions'],
		);
		parent::setUp();
	}

}

$test = new StandardVersionsTest();
$test->run();
