<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\OsDpaAttributes;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Test for IQRF OS or DPA attributes entity
 */
class OsDpaAttributesTest extends TestCase {

	/**
	 * @var OsDpaAttributes Beta IQRF OS or DPA version attributes entity
	 */
	private OsDpaAttributes $entityBeta;

	/**
	 * @var OsDpaAttributes Obsolete IQRF OS or DPA version attributes entity
	 */
	private OsDpaAttributes $entityObsolete;

	/**
	 * @var OsDpaAttributes Stable IQRF OS or DPA version attributes entity
	 */
	private OsDpaAttributes $entityStable;

	/**
	 * Tests the function to create the entity from integer
	 */
	public function testFromIntWithMask(): void {
		Assert::equal($this->entityBeta, OsDpaAttributes::fromInt(1, 0b11));
		Assert::equal($this->entityObsolete, OsDpaAttributes::fromInt(2, 0b10));
		Assert::equal($this->entityStable, OsDpaAttributes::fromInt(0, 0b01));
	}

	/**
	 * Tests the function to create the entity from integer
	 */
	public function testFromIntWithoutMask(): void {
		Assert::equal($this->entityBeta, OsDpaAttributes::fromInt(1));
	}

	/**
	 * Tests the function to check if the version is beta version
	 */
	public function testIsBeta(): void {
		Assert::true($this->entityBeta->beta);
		Assert::null($this->entityObsolete->beta);
		Assert::false($this->entityStable->beta);
	}

	/**
	 * Tests the function to check if the version is obsolete version
	 */
	public function testIsObsolete(): void {
		Assert::false($this->entityBeta->obsolete);
		Assert::true($this->entityObsolete->obsolete);
		Assert::null($this->entityStable->obsolete);
	}

	/**
	 * Tests the function to serialize the entity into query parameters
	 */
	public function testToQueryParameters(): void {
		Assert::same(['beta' => 'true', 'obsolete' => 'false'], $this->entityBeta->toQueryParameters());
		Assert::same(['obsolete' => 'true'], $this->entityObsolete->toQueryParameters());
		Assert::same(['beta' => 'false'], $this->entityStable->toQueryParameters());
	}

	/**
	 * Tests the function to serialize the entity into JSON
	 */
	public function testJsonSerialize(): void {
		Assert::same(['beta' => true, 'obsolete' => false], $this->entityBeta->jsonSerialize());
		Assert::same(['beta' => null, 'obsolete' => true], $this->entityObsolete->jsonSerialize());
		Assert::same(['beta' => false, 'obsolete' => null], $this->entityStable->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		parent::setUp();
		$this->entityBeta = new OsDpaAttributes(true, false);
		$this->entityObsolete = new OsDpaAttributes(null, true);
		$this->entityStable = new OsDpaAttributes(false, null);
	}

}

$test = new OsDpaAttributesTest();
$test->run();
