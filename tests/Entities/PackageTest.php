<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\Package;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for package entity
 */
class PackageTest extends TestCase {

	/**
	 * @var array{
	 *     packageID: int,
	 *     hwpid: int,
	 *     hwpidVer: int,
	 *     handlerUrl: string,
	 *     handlerHash: string,
	 *     os: string,
	 *     dpa: string,
	 *     notes: string,
	 * } API response
	 */
	private array $array = [
		'packageID' => 122,
		'hwpid' => 1026,
		'hwpidVer' => 2,
		'handlerUrl' => 'https://repository.iqrfalliance.org/download/handlers/0402_0002_DDC-SE+RE.hex',
		'handlerHash' => '3AC2E740A4D799E841AF452F2665A678F1DAA69349C2F1C6212AE8424E61E8E9',
		'os' => '08C8',
		'dpa' => '0401',
		'notes' => 'Initial release',
	];

	/**
	 * @var Package Package entity
	 */
	private Package $entity;

	/**
	 * Tests the function to get the package ID
	 */
	public function testGetId(): void {
		Assert::same($this->array['packageID'], $this->entity->id);
	}

	/**
	 * Tests the function to get the package HWPID
	 */
	public function testGetHwpid(): void {
		Assert::same($this->array['hwpid'], $this->entity->hwpid);
	}

	/**
	 * Tests the function to get the package HWPID version
	 */
	public function testGetHwpidVer(): void {
		Assert::same($this->array['hwpidVer'], $this->entity->hwpidVer);
	}

	/**
	 * Tests the function to get the package custom handler URL
	 */
	public function testGetHandlerUrl(): void {
		Assert::same($this->array['handlerUrl'], $this->entity->handlerUrl);
	}

	/**
	 * Tests the function to get the package custom handler URL
	 */
	public function testGetHandlerHash(): void {
		Assert::same($this->array['handlerHash'], $this->entity->handlerHash);
	}

	/**
	 * Tests the function to get the IQRF OS build
	 */
	public function testGetOs(): void {
		Assert::same($this->array['os'], $this->entity->os);
	}

	/**
	 * Tests the function to get the DPA version
	 */
	public function testGetDpa(): void {
		Assert::same($this->array['dpa'], $this->entity->dpa->getRaw());
	}

	/**
	 * Tests the function to get the package notes
	 */
	public function testGetNotes(): void {
		Assert::same($this->array['notes'], $this->entity->notes);
	}

	/**
	 * Tests the function to create a new package entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Package::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the package entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 122,
			'hwpid' => 1026,
			'hwpidVer' => 2,
			'handlerUrl' => 'https://repository.iqrfalliance.org/download/handlers/0402_0002_DDC-SE+RE.hex',
			'handlerHash' => '3AC2E740A4D799E841AF452F2665A678F1DAA69349C2F1C6212AE8424E61E8E9',
			'os' => '08C8',
			'dpa' => '4.01',
			'notes' => 'Initial release',
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Package(
			id: $this->array['packageID'],
			hwpid: $this->array['hwpid'],
			hwpidVer: $this->array['hwpidVer'],
			handlerUrl: $this->array['handlerUrl'],
			handlerHash: $this->array['handlerHash'],
			os: $this->array['os'],
			dpa: new DpaVersion($this->array['dpa']),
			notes: $this->array['notes'],
		);
		parent::setUp();
	}

}

$test = new PackageTest();
$test->run();
