<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TrSeries;
use Iqrf\Repository\Enums\TrDSeries;
use Iqrf\Repository\Enums\TrGSeries;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for TR series entity
 */
class TrSeriesTest extends TestCase {

	/**
	 * @var array{
	 *     mcuMIcode: int,
	 *     seriesMIcode: int,
	 *     seriesImage: string,
	 * } API response
	 */
	private array $arrayDSeries = [
		'mcuMIcode' => 4,
		'seriesMIcode' => 2,
		'seriesImage' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
	];

	/**
	 * @var array{
	 *     mcuMIcode: int,
	 *     seriesMIcode: int,
	 *     seriesImage: string,
	 * } API response
	 */
	private array $arrayGSeries = [
		'mcuMIcode' => 5,
		'seriesMIcode' => 11,
		'seriesImage' => 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76GA_persp.png',
	];

	/**
	 * @var TrSeries TR-D series entity
	 */
	private TrSeries $entityDSeries;

	/**
	 * @var TrSeries TR-G series entity
	 */
	private TrSeries $entityGSeries;

	/**
	 * Tests the function to get TR-D series
	 */
	public function testGetSeriesTrD(): void {
		$expected = TrDSeries::from($this->arrayDSeries['seriesMIcode']);
		Assert::same($expected, $this->entityDSeries->series);
	}

	/**
	 * Tests the function to get TR-G series
	 */
	public function testGetSeriesTrG(): void {
		$expected = TrGSeries::from($this->arrayGSeries['seriesMIcode']);
		Assert::same($expected, $this->entityGSeries->series);
	}

	/**
	 * Tests the function to get TR-D series image
	 */
	public function testGetImageTrD(): void {
		Assert::same($this->arrayDSeries['seriesImage'], $this->entityDSeries->imageUrl);
	}

	/**
	 * Tests the function to get TR-G series image
	 */
	public function testGetImageTrG(): void {
		Assert::same($this->arrayGSeries['seriesImage'], $this->entityGSeries->imageUrl);
	}

	/**
	 * Tests the function to create a new TrSeries entity from API response with TR-D series
	 */
	public function testFromApiResponseTrDseries(): void {
		Assert::equal($this->entityDSeries, TrSeries::fromApiResponse($this->arrayDSeries));
	}

	/**
	 * Tests the function to create a new TrSeries entity from API response with TR-G series
	 */
	public function testFromApiResponseTrGseries(): void {
		Assert::equal($this->entityGSeries, TrSeries::fromApiResponse($this->arrayGSeries));
	}

	/**
	 * Tests the function to serialize the TrSeries entity into JSON with TR-D series
	 */
	public function testJsonSerializeTrDseries(): void {
		$expected = [
			'value' => 2,
			'name' => 'TR-72D',
			'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
		];
		Assert::equal($expected, $this->entityDSeries->jsonSerialize());
	}

	/**
	 * Tests the function to serialize the TrSeries entity into JSON with TR-G series
	 */
	public function testJsonSerializeTrGseries(): void {
		$expected = [
			'value' => 11,
			'name' => 'TR-76G',
			'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76GA_persp.png',
		];
		Assert::equal($expected, $this->entityGSeries->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entityDSeries = new TrSeries(
			series: TrDSeries::TR_72D,
			imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA_persp.png',
		);
		$this->entityGSeries = new TrSeries(
			series: TrGSeries::TR_76G,
			imageUrl: 'https://repository.iqrfalliance.org/trpictures/TR76G/TR76GA_persp.png',
		);
		parent::setUp();
	}

}

$test = new TrSeriesTest();
$test->run();
