<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\PackageDetail;
use Iqrf\Repository\Entities\StandardDetail;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for detailed package entity
 */
class PackageDetailTest extends TestCase {

	/**
	 * @var array{
	 *     packageID: int,
	 *     hwpid: int,
	 *     hwpidVer: int,
	 *     handlerUrl: string,
	 *     handlerHash: string,
	 *     os: string,
	 *     dpa: string,
	 *     notes: string,
	 *     driver: string,
	 *     standards: array<array{
	 *         version: int,
	 *         versionFlags: int,
	 *         driver: string,
	 *         notes: string,
	 *         standardID: int,
	 *         name: string
	 *     }>
	 * } API response
	 */
	private array $array = [
		'packageID' => 122,
		'hwpid' => 1026,
		'hwpidVer' => 2,
		'handlerUrl' => 'https://repository.iqrfalliance.org/download/handlers/0402_0002_DDC-SE+RE.hex',
		'handlerHash' => '3AC2E740A4D799E841AF452F2665A678F1DAA69349C2F1C6212AE8424E61E8E9',
		'os' => '08C8',
		'dpa' => '0401',
		'notes' => 'Initial release',
		'driver' => 'Some JS code...',
		'standards' => [
			[
				'version' => 15,
				'versionFlags' => 0,
				'driver' => 'Some JS code...',
				'notes' => 'Some note',
				'standardID' => 94,
				'name' => 'IQRF: Sensor',
			],
		],
	];

	/**
	 * @var array<StandardDetail> Standard entity
	 */
	private array $standards;

	/**
	 * @var PackageDetail Detailed package entity
	 */
	private PackageDetail $entity;

	/**
	 * Tests the function to create a new package entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, PackageDetail::fromApiResponse($this->array));
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->standards = [new StandardDetail(
			id: 94,
			name: 'IQRF: Sensor',
			version: 15,
			versionFlags: 0,
			driver: 'Some JS code...',
			notes: 'Some note',
		)];
		$this->entity = new PackageDetail(
			id: $this->array['packageID'],
			hwpid: $this->array['hwpid'],
			hwpidVer: $this->array['hwpidVer'],
			handlerUrl: $this->array['handlerUrl'],
			handlerHash: $this->array['handlerHash'],
			os: $this->array['os'],
			dpa: new DpaVersion($this->array['dpa']),
			notes: $this->array['notes'],
			driver: $this->array['driver'],
			standards: $this->standards,
		);
		parent::setUp();
	}

}

$test = new PackageDetailTest();
$test->run();
