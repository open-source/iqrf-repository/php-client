<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Dpa;
use Iqrf\Repository\Entities\DpaVersion;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Nette\Utils\FileSystem;
use Nette\Utils\Json;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for DPA entity
 */
class DpaTest extends TestCase {

	/**
	 * @var array{
	 *      version: string,
	 *      attributes: int,
	 *      downloadPath: string,
	 *  } API response
	 */
	private array $array;

	/**
	 * @var OsDpaAttributes DPA version attributes
	 */
	private OsDpaAttributes $attributes;

	/**
	 * @var Dpa DPA version entity
	 */
	private Dpa $entity;

	/**
	 * Tests the function to get the DPA version
	 */
	public function testGetVersion(): void {
		Assert::same($this->array['version'], $this->entity->version->getRaw());
	}

	/**
	 * Tests the function to get the DPA version (pretty formatting)
	 */
	public function testGetVersionPretty(): void {
		Assert::same('4.15', $this->entity->version->getPretty());
	}

	/**
	 * Tests the function to get the version attributes
	 */
	public function testGetAttributes(): void {
		Assert::same($this->attributes, $this->entity->attributes);
	}

	/**
	 * Tests the function to get the downloadPath
	 */
	public function testGetDownloadPath(): void {
		Assert::same($this->array['downloadPath'], $this->entity->downloadPath);
	}

	/**
	 * Tests the function to create a new DPA entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Dpa::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize DPA entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'version' => '4.15',
			'attributes' => [
				'beta' => false,
				'obsolete' => false,
			],
			'downloadPath' => 'https://repository.iqrfalliance.org/download/dpa/4.15',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->array = Json::decode(FileSystem::read(__DIR__ . '/../data/dpa/0415.json'), forceArrays: true)[0];
		$this->attributes = new OsDpaAttributes(false, false);
		$this->entity = new Dpa(
			version: new DpaVersion($this->array['version']),
			attributes: $this->attributes,
			downloadPath: $this->array['downloadPath'],
		);
		parent::setUp();
	}

}

$test = new DpaTest();
$test->run();
