<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Quantity;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for Quantity entity
 */
class QuantityTest extends TestCase {

	/**
	 * @var array{
	 *     id: string,
	 *     name: string,
	 *     iqrfName: string,
	 *     shortName: string,
	 *     unit: string,
	 *     decimalPlaces: int,
	 *     frcs: array<int>,
	 *     helpPage: string,
	 *     idValue: int,
	 *     width: int,
	 *     idDriver: string,
	 * } API response
	 */
	private array $array = [
		'id' => 'TEMPERATURE',
		'name' => 'Temperature',
		'iqrfName' => 'Temperature',
		'shortName' => 'T',
		'unit' => '°C',
		'decimalPlaces' => 4,
		'frcs' => [
			144,
			224,
		],
		'helpPage' => '0x01-temperature.html',
		'idValue' => 1,
		'width' => 2,
		'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE',
	];

	/**
	 * @var array{
	 *     id: string,
	 *     name: string,
	 *     iqrfName: string,
	 *     shortName: string,
	 *     unit: string,
	 *     decimalPlaces: int,
	 *     frcs: array<int>,
	 *     helpPage: string,
	 *     idValue: int,
	 *     width: int,
	 *     idDriver: string,
	 * } API response with unknown unit
	 */
	private array $arrayUnitUnknown = [
		'id' => 'BINARYDATA7',
		'name' => 'Binary data7',
		'iqrfName' => 'Binary data7',
		'shortName' => 'bin7',
		'unit' => '?',
		'decimalPlaces' => 0,
		'frcs' => [
			16,
			144,
		],
		'helpPage' => '0x81-binary-data7.html',
		'idValue' => 129,
		'width' => 1,
		'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7',
	];

	/**
	 * @var array{
	 *     id: string,
	 *     name: string,
	 *     iqrfName: string,
	 *     shortName: string,
	 *     unit: string,
	 *     decimalPlaces: int,
	 *     frcs: array<int>,
	 *     helpPage: string,
	 *     idValue: int,
	 *     width: int,
	 *     idDriver: string,
	 * } API response with empty unit
	 */
	private array $arrayUnitEmpty = [
		'id' => 'PH',
		'name' => 'pH',
		'iqrfName' => 'pH',
		'shortName' => 'pH',
		'unit' => '',
		'decimalPlaces' => 4,
		'frcs' => [
			144,
		],
		'helpPage' => '0x84-ph.html',
		'idValue' => 132,
		'width' => 1,
		'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_PH',
	];

	/**
	 * @var Quantity Entity
	 */
	private Quantity $entity;

	/**
	 * @var Quantity Entity with unknown unit
	 */
	private Quantity $entityUnitUnknown;

	/**
	 * @var Quantity Entity with empty unit
	 */
	private Quantity $entityUnitEmpty;

	/**
	 * Tests the function to get quantity identifier string
	 */
	public function testGetId(): void {
		Assert::same($this->array['id'], $this->entity->id);
	}

	/**
	 * Tests the function to get quantity name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to get quantity iqrf name
	 */
	public function testGetIqrfName(): void {
		Assert::same($this->array['iqrfName'], $this->entity->iqrfName);
	}

	/**
	 * Tests the function to get quantity short name
	 */
	public function testGetShortName(): void {
		Assert::same($this->array['shortName'], $this->entity->shortName);
	}

	/**
	 * Tests the function to get quantity unit
	 */
	public function testGetUnit(): void {
		Assert::same($this->array['unit'], $this->entity->unit);
	}

	/**
	 * Tests the function to get quantity unit with unknown unit
	 */
	public function testGetUnitUnknown(): void {
		Assert::null($this->entityUnitUnknown->unit);
	}

	/**
	 * Tests the function to get quantity unit with empty unit
	 */
	public function testGetUnitEmpty(): void {
		Assert::null($this->entityUnitEmpty->unit);
	}

	/**
	 * Tests the function to get quantity decimal places
	 */
	public function testGetDecimalPlaces(): void {
		Assert::same($this->array['decimalPlaces'], $this->entity->decimalPlaces);
	}

	/**
	 * Tests the function to get implemented quantity FRC commands
	 */
	public function testGetFrcs(): void {
		Assert::same($this->array['frcs'], $this->entity->frcs);
	}

	/**
	 * Tests the function to get quantity help page URI
	 */
	public function testGetHelpPage(): void {
		Assert::same('https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/' . $this->array['helpPage'], $this->entity->helpPage);
	}

	/**
	 * Tests the function to get quantity ID (IQRF Standard Sensor Type)
	 */
	public function testGetIdValue(): void {
		Assert::same($this->array['idValue'], $this->entity->idValue);
	}

	/**
	 * Tests the function to get quantity data width
	 */
	public function testGetWidth(): void {
		Assert::same($this->array['width'], $this->entity->width);
	}

	/**
	 * Tests the function to get quantity JS driver member name
	 */
	public function testGetIdDriver(): void {
		Assert::same($this->array['idDriver'], $this->entity->idDriver);
	}

	/**
	 * Tests the function to create a new Quantity entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Quantity::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to create a new Quantity entity from the API response with unknown unit
	 */
	public function testFromApiResponseUnitUnknown(): void {
		Assert::equal($this->entityUnitUnknown, Quantity::fromApiResponse($this->arrayUnitUnknown));
	}

	/**
	 * Tests the function to create a new Quantity entity from the API Response with empty unit
	 */
	public function testFromApiResponseUnitEmpty(): void {
		Assert::equal($this->entityUnitEmpty, Quantity::fromApiResponse($this->arrayUnitEmpty));
	}

	/**
	 * Tests the function to serialize the Quantity entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 'TEMPERATURE',
			'name' => 'Temperature',
			'iqrfName' => 'Temperature',
			'shortName' => 'T',
			'unit' => '°C',
			'decimalPlaces' => 4,
			'frcs' => [
				144,
				224,
			],
			'helpPage' => 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x01-temperature.html',
			'idValue' => 1,
			'width' => 2,
			'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE',
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Tests the function to serialize the Quantity entity with unknown unit into JSON
	 */
	public function testJsonSerializeUnitUnknown(): void {
		$expected = [
			'id' => 'BINARYDATA7',
			'name' => 'Binary data7',
			'iqrfName' => 'Binary data7',
			'shortName' => 'bin7',
			'unit' => null,
			'decimalPlaces' => 0,
			'frcs' => [
				16,
				144,
			],
			'helpPage' => 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x81-binary-data7.html',
			'idValue' => 129,
			'width' => 1,
			'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7',
		];
		Assert::same($expected, $this->entityUnitUnknown->jsonSerialize());
	}

	/**
	 * Tests the function to serialize the Quantitty entity with empty unit into JSON
	 */
	public function testJsonSerializeUnitEmpty(): void {
		$expected = [
			'id' => 'PH',
			'name' => 'pH',
			'iqrfName' => 'pH',
			'shortName' => 'pH',
			'unit' => null,
			'decimalPlaces' => 4,
			'frcs' => [
				144,
			],
			'helpPage' => 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x84-ph.html',
			'idValue' => 132,
			'width' => 1,
			'idDriver' => 'iqrf.sensor.STD_SENSOR_TYPE_PH',
		];
		Assert::same($expected, $this->entityUnitEmpty->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Quantity(
			id: $this->array['id'],
			name: $this->array['name'],
			iqrfName: $this->array['iqrfName'],
			shortName: $this->array['shortName'],
			unit: $this->array['unit'],
			decimalPlaces: $this->array['decimalPlaces'],
			frcs: $this->array['frcs'],
			helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x01-temperature.html',
			idValue: $this->array['idValue'],
			width: $this->array['width'],
			idDriver: $this->array['idDriver'],
		);
		$this->entityUnitUnknown = new Quantity(
			id: $this->arrayUnitUnknown['id'],
			name: $this->arrayUnitUnknown['name'],
			iqrfName: $this->arrayUnitUnknown['iqrfName'],
			shortName: $this->arrayUnitUnknown['shortName'],
			unit: null,
			decimalPlaces: $this->arrayUnitUnknown['decimalPlaces'],
			frcs: $this->arrayUnitUnknown['frcs'],
			helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x81-binary-data7.html',
			idValue: $this->arrayUnitUnknown['idValue'],
			width: $this->arrayUnitUnknown['width'],
			idDriver: $this->arrayUnitUnknown['idDriver'],
		);
		$this->entityUnitEmpty = new Quantity(
			id: $this->arrayUnitEmpty['id'],
			name: $this->arrayUnitEmpty['name'],
			iqrfName: $this->arrayUnitEmpty['iqrfName'],
			shortName: $this->arrayUnitEmpty['shortName'],
			unit: null,
			decimalPlaces: $this->arrayUnitEmpty['decimalPlaces'],
			frcs: $this->arrayUnitEmpty['frcs'],
			helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/0x84-ph.html',
			idValue: $this->arrayUnitEmpty['idValue'],
			width: $this->arrayUnitEmpty['width'],
			idDriver: $this->arrayUnitEmpty['idDriver'],
		);
		parent::setUp();
	}

}

$test = new QuantityTest();
$test->run();
