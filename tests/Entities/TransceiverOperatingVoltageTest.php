<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverOperatingVoltage;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver operating voltage entity
 */
class TransceiverOperatingVoltageTest extends TestCase {

	/**
	 * @var array{
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 * } API response
	 */
	private array $array = [
		'operatingVoltageMin' => 2.9,
		'operatingVoltageMax' => 3.4,
		'operatingVoltageNominal' => 3.0,
	];

	/**
	 * @var TransceiverOperatingVoltage Transceiver operating voltage entity
	 */
	private TransceiverOperatingVoltage $entity;

	/**
	 * Tests the function to get minimum operating voltage
	 */
	public function testGetMinimumOperatingVoltage(): void {
		Assert::equal($this->array['operatingVoltageMin'], $this->entity->min);
	}

	/**
	 * Tests the function to get maximum operating voltage
	 */
	public function testGetMaximumOperatingVoltage(): void {
		Assert::equal($this->array['operatingVoltageMax'], $this->entity->max);
	}

	/**
	 * Tests the function to get nominal operating voltage
	 */
	public function testGetNominalOperatingVoltage(): void {
		Assert::equal($this->array['operatingVoltageNominal'], $this->entity->nominal);
	}

	/**
	 * Tests the function to create a new TransceiverOperatingVoltage entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverOperatingVoltage::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverOperatingVoltage entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'min' => 2.9,
			'max' => 3.4,
			'nominal' => 3.0,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverOperatingVoltage(
			min: $this->array['operatingVoltageMin'],
			max: $this->array['operatingVoltageMax'],
			nominal: $this->array['operatingVoltageNominal'],
		);
		parent::setUp();
	}

}

$test = new TransceiverOperatingVoltageTest();
$test->run();
