<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverOperatingVoltage;
use Iqrf\Repository\Entities\TransceiverSupplyVoltage;
use Iqrf\Repository\Entities\TransceiverVoltage;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver voltage entity
 */
class TransceiverVoltageTest extends TestCase {

	/**
	 * @var array{
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 *     supplyVoltageMin: float,
	 *     supplyVoltageMax: float,
	 * } API response
	 */
	private array $array = [
		'operatingVoltageMin' => 2.9,
		'operatingVoltageMax' => 3.4,
		'operatingVoltageNominal' => 3.0,
		'supplyVoltageMin' => 3.1,
		'supplyVoltageMax' => 5.3,
	];

	/**
	 * @var TransceiverVoltage Transceiver voltage entity
	 */
	private TransceiverVoltage $entity;

	/**
	 * Tests the function to get minimum operating voltage
	 */
	public function testGetMinimumOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageMin'], $this->entity->operating->min);
	}

	/**
	 * Tests the function to get maximum operating voltage
	 */
	public function testGetMaximumOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageMax'], $this->entity->operating->max);
	}

	/**
	 * Tests the function to get nominal operating voltage
	 */
	public function testGetNominalOperatingVoltage(): void {
		Assert::same($this->array['operatingVoltageNominal'], $this->entity->operating->nominal);
	}

	/**
	 * Tests the function to get minimum supply voltage
	 */
	public function testGetMinimumSupplyVoltage(): void {
		Assert::same($this->array['supplyVoltageMin'], $this->entity->supply->min);
	}

	/**
	 * Tests the function to get maximum supply voltage
	 */
	public function testGetMaximumSupplyVoltage(): void {
		Assert::same($this->array['supplyVoltageMax'], $this->entity->supply->max);
	}

	/**
	 * Tests the function to create a new TransceiverVoltage entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverVoltage::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverVoltage entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'operating' => [
				'min' => 2.9,
				'max' => 3.4,
				'nominal' => 3.0,
			],
			'supply' => [
				'min' => 3.1,
				'max' => 5.3,
			],
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverVoltage(
			operating: new TransceiverOperatingVoltage(
				min: $this->array['operatingVoltageMin'],
				max: $this->array['operatingVoltageMax'],
				nominal: $this->array['operatingVoltageNominal'],
			),
			supply: new TransceiverSupplyVoltage(
				min: $this->array['supplyVoltageMin'],
				max: $this->array['supplyVoltageMax'],
			),
		);
		parent::setUp();
	}

}

$test = new TransceiverVoltageTest();
$test->run();
