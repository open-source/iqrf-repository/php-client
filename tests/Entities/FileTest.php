<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use DateTime;
use Iqrf\Repository\Entities\File;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for File entity
 */
class FileTest extends TestCase {

	/**
	 * @var array{
	 *     name: string,
	 *     size: int,
	 *     date: int,
	 *     sha256: string,
	 * } API response
	 */
	private array $array = [
		'name' => './DPA-Coordinator-SPI-7xD-V403-190612.iqrf',
		'size' => 16187,
		'date' => 1560338342,
		'sha256' => '632351d02aa35038299a198f6cd70be6039d876f5ecadc2822db372bd99619ec',
	];

	/**
	 * @var DateTime File creation timestamp
	 */
	private DateTime $date;

	/**
	 * @var File File entity
	 */
	private File $entity;

	/**
	 * @var string File name
	 */
	private string $name = 'DPA-Coordinator-SPI-7xD-V403-190612.iqrf';

	/**
	 * @var string File SHA256 hash
	 */
	private string $sha256 = '632351d02aa35038299a198f6cd70be6039d876f5ecadc2822db372bd99619ec';

	/**
	 * @var int File size
	 */
	private int $size = 16187;

	/**
	 * Tests the function to create a new File entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, File::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to get the file creation date
	 */
	public function testGetDate(): void {
		Assert::equal($this->date, $this->entity->date);
	}

	/**
	 * Tests the function to get the file name
	 */
	public function testGetName(): void {
		Assert::same($this->name, $this->entity->name);
	}

	/**
	 * Tests the function to get the file SHA256 hash
	 */
	public function testGetSha256(): void {
		Assert::same($this->sha256, $this->entity->sha256);
	}

	/**
	 * Tests the function to get the file size
	 */
	public function testGetSize(): void {
		Assert::same($this->size, $this->entity->size);
	}

	/**
	 * Tests the function to serialize the File entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'name' => $this->name,
			'size' => $this->size,
			'creationDate' => '2019-06-12T11:19:02+00:00',
			'sha256' => $this->sha256,
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$name = './' . $this->name;
		$this->date = (new DateTime())->setTimestamp(1560338342);
		$this->entity = new File(
			name: $name,
			size: $this->size,
			date: $this->date,
			sha256: $this->sha256,
		);
	}

}

$test = new FileTest();
$test->run();
