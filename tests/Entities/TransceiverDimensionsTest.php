<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverDimensions;
use Iqrf\Repository\Exceptions\TransceiverDimensionsInvalid;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver dimensions entity
 */
class TransceiverDimensionsTest extends TestCase {

	/**
	 * @var array{
	 *     dimensions: string,
	 * } API response
	 */
	private array $array = [
		'dimensions' => '14.9×3.3×25.1',
	];

	/**
	 * @var array{
	 *     width: float,
	 *     height: float,
	 *     depth: float,
	 * } Parsed API response dimensions
	 */
	private array $parsedArray = [
		'width' => 14.9,
		'height' => 3.3,
		'depth' => 25.1,
	];

	/**
	 * @var TransceiverDimensions Transceiver dimensions entity
	 */
	private TransceiverDimensions $entity;

	/**
	 * Tests the function to get transceiver width
	 */
	public function testGetWidth(): void {
		Assert::equal($this->parsedArray['width'], $this->entity->width);
	}

	/**
	 * Tests the function to get transceiver height
	 */
	public function testGetHeight(): void {
		Assert::same($this->parsedArray['height'], $this->entity->height);
	}

	/**
	 * Tests the function to get transceiver depth
	 */
	public function testGetDepth(): void {
		Assert::same($this->parsedArray['depth'], $this->entity->depth);
	}

	/**
	 * Tests the function to create a new TransceiverDimensions entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverDimensions::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to create a new TransceiverDimensions entity from API response containing invalid dimensions data
	 */
	public function testFromApiResponseInvalidDimensions(): void {
		Assert::throws(function (): void {
			TransceiverDimensions::fromApiResponse([
				'dimensions' => 'invalidData',
			]);
		}, TransceiverDimensionsInvalid::class);
	}

	/**
	 * Tests the function to serialize the TransceiverDimensions entity into JSON
	 */
	public function testJsonSerialize(): void {
		Assert::equal($this->parsedArray, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverDimensions(
			width: $this->parsedArray['width'],
			height: $this->parsedArray['height'],
			depth: $this->parsedArray['depth'],
		);
		parent::setUp();
	}

}

$test = new TransceiverDimensionsTest();
$test->run();
