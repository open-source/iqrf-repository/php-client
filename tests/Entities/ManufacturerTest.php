<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Manufacturer;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for manufacturer entity
 */
class ManufacturerTest extends TestCase {

	/**
	 * @var array{
	 *     manufacturerID: int,
	 *     companyID: int,
	 *     name: string,
	 * } API response
	 */
	private array $array = [
		'manufacturerID' => 2,
		'companyID' => 2,
		'name' => 'IQRF Tech s.r.o.',
	];

	/**
	 * @var Manufacturer Manufacturer entity
	 */
	private Manufacturer $entity;

	/**
	 * Tests the function to get the manufacturer ID
	 */
	public function testGetId(): void {
		Assert::same($this->array['manufacturerID'], $this->entity->id);
	}

	/**
	 * Tests the function to get the company ID
	 */
	public function testGetCompanyId(): void {
		Assert::same($this->array['companyID'], $this->entity->companyId);
	}

	/**
	 * Tests the function to get the manufacturer name
	 */
	public function testGetName(): void {
		Assert::same($this->array['name'], $this->entity->name);
	}

	/**
	 * Tests the function to create a new manufacturer entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Manufacturer::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the Manufacturer entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'id' => 2,
			'name' => 'IQRF Tech s.r.o.',
			'companyId' => 2,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new Manufacturer(
			id: $this->array['manufacturerID'],
			companyId: $this->array['companyID'],
			name: $this->array['name'],
		);
		parent::setUp();
	}

}

$test = new ManufacturerTest();
$test->run();
