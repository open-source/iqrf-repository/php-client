<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverCurrentRfRx;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver RF RX current entity
 */
class TransceiverCurrentRfRxTest extends TestCase {

	/**
	 * @var array{
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 * } API response
	 */
	private array $array = [
		'currentRfRxStd' => 12100.0,
		'currentRfRxLp' => 260.0,
		'currentRfRxXlp' => 18.5,
	];

	/**
	 * @var TransceiverCurrentRfRx Transceiver RF RX current entity
	 */
	private TransceiverCurrentRfRx $entity;

	/**
	 * Tests the function to get RF RX STD current
	 */
	public function testGetStdCurrent(): void {
		Assert::equal($this->array['currentRfRxStd'], $this->entity->std);
	}

	/**
	 * Tests the function to get RF RX LP current
	 */
	public function testGetLpCurrent(): void {
		Assert::equal($this->array['currentRfRxLp'], $this->entity->lp);
	}

	/**
	 * Tests the function to get RF RX XLP current
	 */
	public function testGetXlpCurrent(): void {
		Assert::equal($this->array['currentRfRxXlp'], $this->entity->xlp);
	}

	/**
	 * Tests the function to create a new TransceiverCurrentRfRx entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverCurrentRfRx::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverCurrentRfRx entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'std' => 12100.0,
			'lp' => 260.0,
			'xlp' => 18.5,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverCurrentRfRx(
			std: $this->array['currentRfRxStd'],
			lp: $this->array['currentRfRxLp'],
			xlp: $this->array['currentRfRxXlp']
		);
		parent::setUp();
	}

}

$test = new TransceiverCurrentRfRxTest();
$test->run();
