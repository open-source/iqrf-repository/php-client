<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\Credentials;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for Credentials entity
 */
class CredentialsTest extends TestCase {

	/**
	 * Username
	 */
	private const USERNAME = 'username';

	/**
	 * Password
	 */
	private const PASSWORD = 'password';

	/**
	 * Tests the function to convert the entity into an array for Guzzle - unset username and password
	 */
	public function testToGuzzleUnsetUsernameAndPassword(): void {
		$entity = new Credentials(null, null);
		Assert::null($entity->toGuzzle());
	}

	/**
	 * Tests the function to convert the entity into an array for Guzzle - unset username
	 */
	public function testToGuzzleUnsetUsername(): void {
		$entity = new Credentials(null, self::PASSWORD);
		Assert::null($entity->toGuzzle());
	}

	/**
	 * Tests the function to convert the entity into an array for Guzzle - empty username
	 */
	public function testToGuzzleEmptyUsername(): void {
		$entity = new Credentials('', self::PASSWORD);
		Assert::null($entity->toGuzzle());
	}

	/**
	 * Tests the function to convert the entity into an array for Guzzle - unset password
	 */
	public function testToGuzzleUnsetPassword(): void {
		$entity = new Credentials(self::USERNAME);
		Assert::same([self::USERNAME, ''], $entity->toGuzzle());
	}

	/**
	 * Tests the function to convert the entity into an array for Guzzle
	 */
	public function testToGuzzle(): void {
		$entity = new Credentials(self::USERNAME, self::PASSWORD);
		Assert::same([self::USERNAME, self::PASSWORD], $entity->toGuzzle());
	}

}

$test = new CredentialsTest();
$test->run();
