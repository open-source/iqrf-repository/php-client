<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use DateTime;
use Iqrf\Repository\Entities\File;
use Iqrf\Repository\Entities\Files;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for Files entity
 */
class FilesTest extends TestCase {

	/**
	 * @var array{
	 *     timestamp: int,
	 *     timestampDateTime?: string,
	 *     version: int,
	 *     files: array<array{
	 *         name: string,
	 *         size: int,
	 *         date: int,
	 *         dateDateTime?: string,
	 *         sha256: string
	 *     }>
	 * } API response
	 */
	private array $array = [
		'timestamp' => 1559641582,
		'version' => 0,
		'files' => [
			[
				'name' => './DPA-Coordinator-SPI-7xD-V402-190603.iqrf',
				'size' => 16187,
				'date' => 1559576647,
				'sha256' => '4f786756b9dcbc237f195146d35eb22556592fe795759a1d4be316395cc05087',
			],
			[
				'name' => './DPA-Coordinator-UART-7xD-V402-190603.iqrf',
				'size' => 17364,
				'date' => 1559576660,
				'sha256' => 'f158fd1e13c1d9f5061a4e794f43f368e4b50e5253a1bc250fbb91fd50403d8b',
			],
		],
	];

	/**
	 * @var Files Files entity
	 */
	private Files $entity;

	/**
	 * @var array<File> Files
	 */
	private array $files = [];

	/**
	 * @var DateTime Timestamp
	 */
	private DateTime $dateTime;

	/**
	 * @var int Format version
	 */
	private int $version = 0;

	/**
	 * Tests the function to create a new Files entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, Files::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the Files entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'timestamp' => '2019-06-04T09:46:22+00:00',
			'version' => 0,
			'files' => [
				[
					'name' => 'DPA-Coordinator-SPI-7xD-V402-190603.iqrf',
					'size' => 16187,
					'creationDate' => '2019-06-03T15:44:07+00:00',
					'sha256' => '4f786756b9dcbc237f195146d35eb22556592fe795759a1d4be316395cc05087',
				],
				[
					'name' => 'DPA-Coordinator-UART-7xD-V402-190603.iqrf',
					'size' => 17364,
					'creationDate' => '2019-06-03T15:44:20+00:00',
					'sha256' => 'f158fd1e13c1d9f5061a4e794f43f368e4b50e5253a1bc250fbb91fd50403d8b',
				],
			],
		];
		Assert::same($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Tests the function to get array of files
	 */
	public function testGetFiles(): void {
		Assert::same($this->files, $this->entity->files);
	}

	/**
	 * Tests the function to get the timestamp
	 */
	public function testGetTimestamp(): void {
		Assert::equal($this->dateTime, $this->entity->timestamp);
	}

	/**
	 * Tests the function to get the format version
	 */
	public function testGetVersion(): void {
		Assert::same($this->version, $this->entity->version);
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->files = [
			File::fromApiResponse($this->array['files'][0]),
			File::fromApiResponse($this->array['files'][1]),
		];
		$this->dateTime = (new DateTime())->setTimestamp(1559641582);
		$this->entity = new Files(
			timestamp: $this->dateTime,
			version: $this->version,
			files: $this->files,
		);
	}

}

$test = new FilesTest();
$test->run();
