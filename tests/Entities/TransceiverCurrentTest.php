<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverCurrent;
use Iqrf\Repository\Entities\TransceiverCurrentRf;
use Iqrf\Repository\Entities\TransceiverCurrentRfRx;
use Iqrf\Repository\Entities\TransceiverCurrentRfTx;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver current entity
 */
class TransceiverCurrentTest extends TestCase {

	/**
	 * @var array{
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 *     currentRfReady: float,
	 *     currentLEDG: float,
	 *     currentLEDR: float,
	 *     currentMcu: float,
	 *     currentSleep: float,
	 *     currentDeepSleep: float,
	 * } API response
	 */
	private array $array = [
		'currentRfTxMin' => 8300.0,
		'currentRfTxMax' => 25000.0,
		'currentRfRxStd' => 12100.0,
		'currentRfRxLp' => 260.0,
		'currentRfRxXlp' => 18.5,
		'currentRfReady' => 2800.0,
		'currentLEDG' => 2000.0,
		'currentLEDR' => 2000.0,
		'currentMcu' => 1950.0,
		'currentSleep' => 2.3,
		'currentDeepSleep' => 1.7,
	];

	/**
	 * @var TransceiverCurrent Transceiver current entity
	 */
	private TransceiverCurrent $entity;

	/**
	 * Tests the function to get minimum RF TX current
	 */
	public function testGetTxMinCurrent(): void {
		Assert::equal($this->array['currentRfTxMin'], $this->entity->rf->tx->min);
	}

	/**
	 * Tests the function to get maximum RF TX current
	 */
	public function testGetTxMaxCurrent(): void {
		Assert::equal($this->array['currentRfTxMax'], $this->entity->rf->tx->max);
	}

	/**
	 * Tests the function to get RF RX STD current
	 */
	public function testGetRxStdCurrent(): void {
		Assert::equal($this->array['currentRfRxStd'], $this->entity->rf->rx->std);
	}

	/**
	 * Tests the function to get RF RX LP current
	 */
	public function testGetRxLpCurrent(): void {
		Assert::equal($this->array['currentRfRxLp'], $this->entity->rf->rx->lp);
	}

	/**
	 * Tests the function to get RF RX XLP current
	 */
	public function testGetRxXlpCurrent(): void {
		Assert::equal($this->array['currentRfRxXlp'], $this->entity->rf->rx->xlp);
	}

	/**
	 * Tests the function to get RF ready current
	 */
	public function testGetReadyCurrent(): void {
		Assert::equal($this->array['currentRfReady'], $this->entity->rf->ready);
	}

	/**
	 * Tests the function to get LEDG current
	 */
	public function testGetLedgCurrent(): void {
		Assert::equal($this->array['currentLEDG'], $this->entity->ledg);
	}

	/**
	 * Tests the function to get LEDR current
	 */
	public function testGetLedrCurrent(): void {
		Assert::equal($this->array['currentLEDR'], $this->entity->ledr);
	}

	/**
	 * Tests the function to get MCU current
	 */
	public function testGetMcuCurrent(): void {
		Assert::equal($this->array['currentMcu'], $this->entity->mcu);
	}

	/**
	 * Tests the function to get sleep current
	 */
	public function testGetSleepCurrent(): void {
		Assert::equal($this->array['currentSleep'], $this->entity->sleep);
	}

	/**
	 * Tests the function to get deep sleep current
	 */
	public function testGetDeepSleepCurrent(): void {
		Assert::equal($this->array['currentDeepSleep'], $this->entity->deepSleep);
	}

	/**
	 * Tests the function to create a new TransceiverCurrent entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverCurrent::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverCurrent entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'rf' => [
				'tx' => [
					'min' => 8300.0,
					'max' => 25000.0,
				],
				'rx' => [
					'std' => 12100.0,
					'lp' => 260.0,
					'xlp' => 18.5,
				],
				'ready' => 2800.0,
			],
			'ledg' => 2000.0,
			'ledr' => 2000.0,
			'mcu' => 1950.0,
			'sleep' => 2.3,
			'deepSleep' => 1.7,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverCurrent(
			rf: new TransceiverCurrentRf(
				tx: new TransceiverCurrentRfTx(
					min: $this->array['currentRfTxMin'],
					max: $this->array['currentRfTxMax'],
				),
				rx: new TransceiverCurrentRfRx(
					std: $this->array['currentRfRxStd'],
					lp: $this->array['currentRfRxLp'],
					xlp: $this->array['currentRfRxXlp'],
				),
				ready: $this->array['currentRfReady'],
			),
			ledg: $this->array['currentLEDG'],
			ledr: $this->array['currentLEDR'],
			mcu: $this->array['currentMcu'],
			sleep: $this->array['currentSleep'],
			deepSleep: $this->array['currentDeepSleep'],
		);
		parent::setUp();
	}

}

$test = new TransceiverCurrentTest();
$test->run();
