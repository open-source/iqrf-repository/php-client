<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use DateTime;
use Iqrf\Repository\Entities\ServerStatus;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for server status entity
 */
class ServerStatusTest extends TestCase {

	/**
	 * @var array{
	 *      apiVersion: int,
	 *      hostname: string,
	 *      user: string,
	 *      buildDateTime: string,
	 *      startDateTime: string,
	 *      dateTime: string,
	 *      databaseChecksum: int,
	 *      databaseChangeDateTime: string,
	 *  } API response
	 */
	private array $array = [
		'apiVersion' => 0,
		'buildDateTime' => '2019-03-15T16:33:32+01:00',
		'databaseChangeDateTime' => '2019-06-28T15:01:48+02:00',
		'databaseChecksum' => -4185524147208511958,
		'dateTime' => '2019-07-17T12:30:31+02:00',
		'hostname' => 'repository.iqrfalliance.org',
		'startDateTime' => '2019-04-28T21:17:16+02:00',
		'user' => 'guest',
	];

	/**
	 * @var ServerStatus Server status entity
	 */
	private ServerStatus $entity;

	/**
	 * Tests the function to get the API version
	 */
	public function testGetApiVersion(): void {
		Assert::same($this->array['apiVersion'], $this->entity->apiVersion);
	}

	/**
	 * Tests the function to get the server hostname
	 */
	public function testGetHostname(): void {
		Assert::same($this->array['hostname'], $this->entity->hostname);
	}

	/**
	 * Tests the function to get the authenticated user name
	 */
	public function testGetUser(): void {
		Assert::same($this->array['user'], $this->entity->user);
	}

	/**
	 * Tests the function to create a new server status entity from the API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, ServerStatus::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to get the server build date and time
	 */
	public function testGetBuildDateTime(): void {
		$expected = new DateTime($this->array['buildDateTime']);
		Assert::equal($expected, $this->entity->buildDateTime);
	}

	/**
	 * Tests the function to get the server startup date and time
	 */
	public function testGetStartDateTime(): void {
		$expected = new DateTime($this->array['startDateTime']);
		Assert::equal($expected, $this->entity->startDateTime);
	}

	/**
	 * Tests the function to get the current date and time
	 */
	public function testGetDateTime(): void {
		$expected = new DateTime($this->array['dateTime']);
		Assert::equal($expected, $this->entity->dateTime);
	}

	/**
	 * Tests the function to get the all database tables checksum
	 */
	public function testGetDatabaseChecksum(): void {
		Assert::same($this->array['databaseChecksum'], $this->entity->databaseChecksum);
	}

	/**
	 * Tests the function to get the last database change date and time
	 */
	public function testGetDatabaseChangeTime(): void {
		$expected = new DateTime($this->array['databaseChangeDateTime']);
		Assert::equal($expected, $this->entity->databaseChangeDateTime);
	}

	/**
	 * Tests the function to serialize the server status entity into JSON
	 */
	public function testJsonSerialize(): void {
		Assert::equal($this->array, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new ServerStatus(
			apiVersion: $this->array['apiVersion'],
			hostname: $this->array['hostname'],
			user: $this->array['user'],
			buildDateTime: new DateTime($this->array['buildDateTime']),
			startDateTime: new DateTime($this->array['startDateTime']),
			dateTime: new DateTime($this->array['dateTime']),
			databaseChecksum: $this->array['databaseChecksum'],
			databaseChangeDateTime: new DateTime($this->array['databaseChangeDateTime']),
		);
		parent::setUp();
	}

}

$test = new ServerStatusTest();
$test->run();
