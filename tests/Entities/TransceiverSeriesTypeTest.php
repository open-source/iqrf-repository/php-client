<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverDimensions;
use Iqrf\Repository\Entities\TransceiverSeriesType;
use Iqrf\Repository\Enums\TransceiverAntenna;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver series type entity
 */
class TransceiverSeriesTypeTest extends TestCase {

	/**
	 * @var array{
	 *     typeName: string,
	 *     temperatureSensor: bool,
	 *     antenna: string,
	 *     dimensions: string,
	 *     image: string
	 * } API response
	 */
	private array $array = [
		'typeName' => 'TR-72A',
		'temperatureSensor' => false,
		'antenna' => 'PCB',
		'dimensions' => '14.9×3.3×31.8',
		'image' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
	];

	/**
	 * @var array{
	 *     width: float,
	 *     height: float,
	 *     depth: float,
	 * } Parsed API response dimensions
	 */
	private array $parsedDimensionsArray = [
		'width' => 14.9,
		'height' => 3.3,
		'depth' => 31.8,
	];

	/**
	 * @var array{
	 *     typeName: string,
	 *     temperatureSensor: bool,
	 *     antenna: string,
	 *     dimensions: string,
	 *     image?: string
	 * } API response without image
	 */
	private array $arrayNoImage = [
		'typeName' => 'TR-72D',
		'temperatureSensor' => false,
		'antenna' => 'Pad',
		'dimensions' => '14.9×3.3×25.1',
	];

	/**
	 * @var TransceiverSeriesType Transceiver series type entity
	 */
	private TransceiverSeriesType $entity;

	/**
	 * @var TransceiverSeriesType Transceiver series type entity without image
	 */
	private TransceiverSeriesType $entityNoImage;

	/**
	 * Tests the function to get transceiver type
	 */
	public function testGetName(): void {
		Assert::same($this->array['typeName'], $this->entity->name);
	}

	/**
	 * Tests the function to get transceiver temperature sensor present
	 */
	public function testGetTemperatureSensor(): void {
		Assert::same($this->array['temperatureSensor'], $this->entity->temperatureSensor);
	}

	/**
	 * Tests the function to get transceiver antenna
	 */
	public function testGetAntenna(): void {
		$expected = TransceiverAntenna::from($this->array['antenna']);
		Assert::same($expected, $this->entity->antenna);
	}

	/**
	 * Tests the function to get transceiver width
	 */
	public function testGetWidth(): void {
		Assert::equal($this->parsedDimensionsArray['width'], $this->entity->dimensions->width);
	}

	/**
	 * Tests the function to get transceiver height
	 */
	public function testGetHeight(): void {
		Assert::same($this->parsedDimensionsArray['height'], $this->entity->dimensions->height);
	}

	/**
	 * Tests the function to get transceiver depth
	 */
	public function testGetDepth(): void {
		Assert::same($this->parsedDimensionsArray['depth'], $this->entity->dimensions->depth);
	}

	/**
	 * Tests the function to get transceiver image
	 */
	public function testGetImage(): void {
		Assert::same($this->array['image'], $this->entity->imageUrl);
	}

	/**
	 * Tests the function to get transceiver image from data without image
	 */
	public function testGetImageNoImage(): void {
		Assert::null($this->entityNoImage->imageUrl);
	}

	/**
	 * Tests the function to create a new TransceiverSeriesType entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverSeriesType::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to create a new TransceiverSeriesType entity from API response without image
	 */
	public function testFromApiResponseNoImage(): void {
		Assert::equal($this->entityNoImage, TransceiverSeriesType::fromApiResponse($this->arrayNoImage));
	}

	/**
	 * Tests the function to serialize the TransceiverSeriesType entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'name' => 'TR-72A',
			'temperatureSensor' => false,
			'antenna' => 'PCB',
			'dimensions' => [
				'width' => 14.9,
				'height' => 3.3,
				'depth' => 31.8,
			],
			'imageUrl' => 'https://repository.iqrfalliance.org/trpictures/TR72D/TR72DA.png',
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Tests the function to serialize the TransceiverSeriesType entity without image into JSON
	 */
	public function testJsonSerializeNoImage(): void {
		$expected = [
			'name' => 'TR-72D',
			'temperatureSensor' => false,
			'antenna' => 'Pad',
			'dimensions' => [
				'width' => 14.9,
				'height' => 3.3,
				'depth' => 25.1,
			],
			'imageUrl' => null,
		];
		Assert::equal($expected, $this->entityNoImage->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverSeriesType(
			name: $this->array['typeName'],
			temperatureSensor: $this->array['temperatureSensor'],
			antenna: TransceiverAntenna::PCB,
			dimensions: new TransceiverDimensions(14.9, 3.3, 31.8),
			imageUrl: $this->array['image'],
		);
		$this->entityNoImage = new TransceiverSeriesType(
			name: $this->arrayNoImage['typeName'],
			temperatureSensor: $this->arrayNoImage['temperatureSensor'],
			antenna: TransceiverAntenna::PAD,
			dimensions: new TransceiverDimensions(14.9, 3.3, 25.1),
		);
		parent::setUp();
	}

}

$test = new TransceiverSeriesTypeTest();
$test->run();
