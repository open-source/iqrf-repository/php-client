<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverCurrentRfTx;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver RF TX current entity
 */
class TransceiverCurrentRfTxTest extends TestCase {

	/**
	 * @var array{
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 * } API response
	 */
	private array $array = [
		'currentRfTxMin' => 8300.0,
		'currentRfTxMax' => 25000.0,
	];

	/**
	 * @var TransceiverCurrentRfTx Transceiver RF TX current entity
	 */
	private TransceiverCurrentRfTx $entity;

	/**
	 * Tests the function to get minimum RF TX current
	 */
	public function testGetMinCurrent(): void {
		Assert::equal($this->array['currentRfTxMin'], $this->entity->min);
	}

	/**
	 * Tests the function to get maximum RF TX current
	 */
	public function testGetMaxCurrent(): void {
		Assert::equal($this->array['currentRfTxMax'], $this->entity->max);
	}

	/**
	 * Tests the function to create a new TransceiverCurrentRfTx entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverCurrentRfTx::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverCurrentRfTx entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'min' => 8300.0,
			'max' => 25000.0,
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverCurrentRfTx(
			min: $this->array['currentRfTxMin'],
			max: $this->array['currentRfTxMax'],
		);
		parent::setUp();
	}

}

$test = new TransceiverCurrentRfTxTest();
$test->run();
