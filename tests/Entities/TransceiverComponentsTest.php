<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @testCase
 */
declare(strict_types = 1);

namespace Tests\Iqrf\Repository\Entities;

use Iqrf\Repository\Entities\TransceiverComponents;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../bootstrap.php';

/**
 * Tests for transceiver components entity
 */
class TransceiverComponentsTest extends TestCase {

	/**
	 * @var array{
	 *     rfChip: string,
	 *     ldo: string,
	 *     temperatureSensorChip: string,
	 * } API response
	 */
	private array $array = [
		'rfChip' => 'Spirit1',
		'ldo' => 'MCP1700',
		'temperatureSensorChip' => 'MCP9808',
	];

	/**
	 * @var TransceiverComponents Transceiver components entity
	 */
	private TransceiverComponents $entity;

	/**
	 * Tests the function to get rf chip component
	 */
	public function testGetRfChip(): void {
		Assert::same($this->array['rfChip'], $this->entity->rfChip);
	}

	/**
	 * Tests the function to get ldo component
	 */
	public function testGetLdo(): void {
		Assert::same($this->array['ldo'], $this->entity->ldo);
	}

	/**
	 * Tests the function to get temperature sensor chip component
	 */
	public function testGetTemperatureSensorChip(): void {
		Assert::same($this->array['temperatureSensorChip'], $this->entity->temperatureSensorChip);
	}

	/**
	 * Tests the function to create a new TransceiverComponents entity from API response
	 */
	public function testFromApiResponse(): void {
		Assert::equal($this->entity, TransceiverComponents::fromApiResponse($this->array));
	}

	/**
	 * Tests the function to serialize the TransceiverComponents entity into JSON
	 */
	public function testJsonSerialize(): void {
		$expected = [
			'rfChip' => 'Spirit1',
			'ldo' => 'MCP1700',
			'temperatureSensorChip' => 'MCP9808',
		];
		Assert::equal($expected, $this->entity->jsonSerialize());
	}

	/**
	 * Sets up the test environment
	 */
	protected function setUp(): void {
		$this->entity = new TransceiverComponents(
			rfChip: $this->array['rfChip'],
			ldo: $this->array['ldo'],
			temperatureSensorChip: $this->array['temperatureSensorChip'],
		);
		parent::setUp();
	}

}

$test = new TransceiverComponentsTest();
$test->run();
