<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Utils;

use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * HTTP(S) client for API
 */
class ApiClient extends HttpClient {

	/**
	 * Sends JSON API request
	 * @param string $path API path
	 * @param array<mixed>|null $query Optional API query
	 * @return array<mixed> JSON API response
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws DataNotFound Requested data not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function sendApiRequest(string $path, ?array $query = null, bool $useCredentials = false): array {
		$url = $this->configuration->getApiEndpoint() . '/' . $path;
		try {
			$content = self::sendRequest($url, $query, $useCredentials);
			return Json::decode($content, forceArrays: true);
		} catch (JsonException) {
			throw new CorruptedData();
		}
	}

}
