<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Iqrf\Repository\Configuration;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * HTTP(S) client
 */
abstract class HttpClient {

	/**
	 * Constructor
	 * @param Configuration $configuration Configuration
	 * @param Client $httpClient HTTP(S) client
	 */
	public function __construct(
		protected readonly Configuration $configuration,
		protected readonly Client $httpClient,
	) {
	}

	/**
	 * Sends HTTP request
	 * @param string $url URL
	 * @param array<mixed>|null $query Optional query parameters
	 * @return string HTTP response
	 * @throws DataNotFound Requested data not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function sendRequest(string $url, ?array $query = null, bool $useCredentials = false): string {
		$options = [];
		if ($query !== null) {
			$options['query'] = $query;
		}
		$credentials = $this->configuration->getCredentials()->toGuzzle();
		if ($useCredentials && $credentials !== null) {
			$options['auth'] = $credentials;
		}
		try {
			return $this->httpClient->get($url, $options)->getBody()->getContents();
		} catch (RequestException $e) {
			if ($e->getCode() === 404) {
				throw new DataNotFound();
			}
			throw new ServiceUnavailable();
		}
	}

}
