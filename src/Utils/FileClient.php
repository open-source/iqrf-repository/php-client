<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Utils;

use BadMethodCallException;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\FileNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * HTTP(S) client for file downloading
 */
class FileClient extends HttpClient {

	/**
	 * @var string|null Base path
	 */
	private ?string $path = null;

	/**
	 * Sets the base path
	 * @param string $path Base path
	 */
	public function setPath(string $path): void {
		$this->path = $path;
	}

	/**
	 * Downloads the file from the IQRF Repository
	 * @param string $path File path
	 * @param bool $useCredentials Use credentials
	 * @return string File content
	 * @throws BadMethodCallException
	 * @throws FileNotFound Requested file not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function getFile(string $path, bool $useCredentials = false): string {
		if ($this->path === null) {
			throw new BadMethodCallException();
		}
		try {
			$url = $this->path . '/' . $path;
			return self::sendRequest($url, null, $useCredentials);
		} catch (DataNotFound) {
			throw new FileNotFound();
		}
	}

}
