<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository;

use Iqrf\Repository\Entities\Credentials;

/**
 * IQRF Repository API integration configuration
 */
class Configuration {

	/**
	 * Constructor
	 * @param string $apiEndpoint IQRF Repository API endpoint URL
	 * @param Credentials $credentials IQRF Repository credentials
	 */
	public function __construct(
		public string $apiEndpoint,
		public Credentials $credentials,
	) {
	}

	/**
	 * Returns the IQRF Repository API endpoint URL
	 * @return string IQRF Repository API endpoint URL
	 */
	public function getApiEndpoint(): string {
		return $this->apiEndpoint;
	}

	/**
	 * Returns the credentials
	 * @return Credentials Credentials
	 */
	public function getCredentials(): Credentials {
		return $this->credentials;
	}

}
