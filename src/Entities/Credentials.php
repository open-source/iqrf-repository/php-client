<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Credentials entity
 */
final class Credentials {

	/**
	 * Constructor
	 * @param string|null $username Username
	 * @param string|null $password Password
	 */
	public function __construct(
		private readonly ?string $username = null,
		private readonly ?string $password = null,
	) {
	}

	/**
	 * Converts the object into an array for Guzzle
	 * @return array<string>|null Credentials for Guzzle
	 */
	public function toGuzzle(): ?array {
		if ($this->username === null || $this->username === '') {
			return null;
		}
		return [$this->username, $this->password ?? ''];
	}

}
