<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Common\FloatValRange;

/**
 * Transceiver RF TX current entity
 */
final class TransceiverCurrentRfTx extends FloatValRange implements \JsonSerializable {

	/**
	 * @param float $min Minimum RF TX current [µA]
	 * @param float $max Maximum RF TX current [µA]
	 */
	public function __construct(
		float $min,
		float $max,
	) {
		parent::__construct($min, $max);
	}

	/**
	 * Creates a new TransceiverCurrentRfTx entity from API Response
	 * @param array{
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 * } $response API Response body
	 * @return TransceiverCurrentRfTx TransceiverCurrentRfTx entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			min: $response['currentRfTxMin'],
			max: $response['currentRfTxMax'],
		);
	}

	/**
	 * Serializes the TransceiverCurrentRfTx entity into JSON
	 * @return array{
	 *     min: float,
	 *     max: float,
	 * } JSON serialized TransceiverCurrentRfTx entity
	 */
	public function jsonSerialize(): array {
		return [
			'min' => $this->min,
			'max' => $this->max,
		];
	}

}
