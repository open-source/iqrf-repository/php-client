<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\ITrSeries;
use Iqrf\Repository\Enums\McuTypes;
use Iqrf\Repository\Enums\TrDSeries;
use Iqrf\Repository\Enums\TrGSeries;

/**
 * TrSeries entity
 */
final class TrSeries implements \JsonSerializable {

	/**
	 * Constructor
	 * @param ITrSeries $series TR series
	 * @param string $imageUrl Image URL
	 */
	public function __construct(
		public readonly ITrSeries $series,
		public readonly string $imageUrl,
	) {
	}

	/**
	 * Creates a new TrSeries entity from API response
	 * @param array{
	 *     seriesMIcode: int,
	 *     mcuMIcode: int,
	 *     seriesImage: string,
	 * } $response API response body
	 */
	public static function fromApiResponse(array $response): self {
		$mcu = McuTypes::from($response['mcuMIcode']);
		$series = $mcu === McuTypes::PIC16LF1938 ? TrDSeries::from($response['seriesMIcode']) : TrGSeries::from($response['seriesMIcode']);
		return new self(
			series: $series,
			imageUrl: $response['seriesImage'],
		);
	}

	/**
	 * Serializes the TrSeries entity into JSON
	 * @return array{
	 *     value: int,
	 *     name: string,
	 *     imageUrl: string,
	 * } JSON serialized TrSeries entity
	 */
	public function jsonSerialize(): array {
		return [
			'value' => (int) $this->series->value,
			'name' => $this->series->toString(),
			'imageUrl' => $this->imageUrl,
		];
	}

}
