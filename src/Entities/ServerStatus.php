<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use DateTime;
use Iqrf\Repository\Exceptions\CorruptedData;
use JsonSerializable;
use Throwable;

/**
 * Server status entity
 */
final class ServerStatus implements JsonSerializable {

	/**
	 * Constructor
	 * @param int $apiVersion API version
	 * @param string $hostname Server hostname
	 * @param string $user Authenticated user name
	 * @param DateTime $buildDateTime Server build date and time
	 * @param DateTime $startDateTime Server startup date and time
	 * @param DateTime $dateTime Current date and time
	 * @param int $databaseChecksum All database tables checksum
	 * @param DateTime $databaseChangeDateTime Last database change date and time
	 */
	public function __construct(
		public readonly int $apiVersion,
		public readonly string $hostname,
		public readonly string $user,
		public readonly DateTime $buildDateTime,
		public readonly DateTime $startDateTime,
		public readonly DateTime $dateTime,
		public readonly int $databaseChecksum,
		public readonly DateTime $databaseChangeDateTime,
	) {
	}

	/**
	 * Creates a new server status entity from the API response
	 * @param array{
	 *     apiVersion: int,
	 *     hostname: string,
	 *     user: string,
	 *     buildDateTime: string,
	 *     startDateTime: string,
	 *     dateTime: string,
	 *     databaseChecksum: int,
	 *     databaseChangeDateTime: string,
	 * } $response API response body
	 * @return ServerStatus Created server status entity
	 * @throws CorruptedData Thrown when the API response data are corrupted
	 */
	public static function fromApiResponse(array $response): self {
		try {
			return new self(
				apiVersion: $response['apiVersion'],
				hostname: $response['hostname'],
				user: $response['user'],
				buildDateTime: new DateTime($response['buildDateTime']),
				startDateTime: new DateTime($response['startDateTime']),
				dateTime: new DateTime($response['dateTime']),
				databaseChecksum: $response['databaseChecksum'],
				databaseChangeDateTime: new DateTime($response['databaseChangeDateTime']),
			);
		} catch (Throwable) {
			throw new CorruptedData();
		}
	}

	/**
	 * Serializes the server status entity into JSON
	 * @return array{
	 *     apiVersion: int,
	 *     hostname: string,
	 *     user: string,
	 *     buildDateTime: string,
	 *     startDateTime: string,
	 *     dateTime: string,
	 *     databaseChecksum: int,
	 *     databaseChangeDateTime: string,
	 * } JSON serialized server status entity
	 */
	public function jsonSerialize(): array {
		return [
			'apiVersion' => $this->apiVersion,
			'hostname' => $this->hostname,
			'user' => $this->user,
			'buildDateTime' => $this->buildDateTime->format('c'),
			'startDateTime' => $this->startDateTime->format('c'),
			'dateTime' => $this->dateTime->format('c'),
			'databaseChecksum' => $this->databaseChecksum,
			'databaseChangeDateTime' => $this->databaseChangeDateTime->format('c'),
		];
	}

}
