<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Detailed package entity
 */
final class PackageDetail {

	/**
	 * Constructor
	 * @param int $id Package ID
	 * @param int $hwpid HWPID
	 * @param int $hwpidVer HWPID version
	 * @param string $handlerUrl Custom handler URL
	 * @param string $handlerHash Custom handler hash
	 * @param string $os IQRF OS build
	 * @param DpaVersion $dpa DPA version
	 * @param string $notes Package notes
	 * @param string $driver Driver code
	 * @param array<StandardDetail> $standards Array of supported standards
	 */
	public function __construct(
		public readonly int $id,
		public readonly int $hwpid,
		public readonly int $hwpidVer,
		public readonly string $handlerUrl,
		public readonly string $handlerHash,
		public readonly string $os,
		public readonly DpaVersion $dpa,
		public readonly string $notes,
		public readonly string $driver,
		public readonly array $standards,
	) {
	}

	/**
	 * Creates a new detailed package entity from the API response
	 * @param array{
	 *     packageID: int,
	 *     hwpid: int,
	 *     hwpidVer: int,
	 *     handlerUrl: string,
	 *     handlerHash: string,
	 *     os: string,
	 *     dpa: string,
	 *     notes: string,
	 *     driver: string,
	 *     standards: array<array{
	 *         standardID: int,
	 *         name: string,
	 *         version: float,
	 *         versionFlags: int,
	 *         driver: string,
	 *         notes: string,
	 *     }>,
	 * } $response API response body
	 * @return PackageDetail Created detailed package entity
	 */
	public static function fromApiResponse(array $response): self {
		$standards = array_map(static fn (array $standard): StandardDetail => StandardDetail::fromApiResponse($standard), $response['standards']);
		return new self(
			id: $response['packageID'],
			hwpid: $response['hwpid'],
			hwpidVer: $response['hwpidVer'],
			handlerUrl: $response['handlerUrl'],
			handlerHash: $response['handlerHash'],
			os: $response['os'],
			dpa: new DpaVersion($response['dpa']),
			notes: $response['notes'],
			driver: $response['driver'],
			standards: $standards,
		);
	}

}
