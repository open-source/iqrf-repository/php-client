<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * Quantity entity
 */
final class Quantity implements JsonSerializable {

	/**
	 * Constructor
	 * @param string $id Quantity identifier string
	 * @param string $name Quantity name
	 * @param string $iqrfName Quantity IQRF name
	 * @param string $shortName Quantity short name
	 * @param string|null $unit Unit of measurement
	 * @param int $decimalPlaces Decimal places of measured value
	 * @param array<int> $frcs Implemented FRC commands
	 * @param string $helpPage Help page URI
	 * @param int $idValue Quantity ID (IQRF Standard Sensor Type)
	 * @param int $width Data width
	 * @param string $idDriver JS driver member name
	 */
	public function __construct(
		public readonly string $id,
		public readonly string $name,
		public readonly string $iqrfName,
		public readonly string $shortName,
		public readonly string|null $unit,
		public readonly int $decimalPlaces,
		public readonly array $frcs,
		public readonly string $helpPage,
		public readonly int $idValue,
		public readonly int $width,
		public readonly string $idDriver,
	) {
	}

	/**
	 * Creates a new Quantity entity from API response
	 * @param array{
	 *     id: string,
	 *     name: string,
	 *     iqrfName: string,
	 *     shortName: string,
	 *     unit: string,
	 *     decimalPlaces: int,
	 *     frcs: array<int>,
	 *     helpPage: string,
	 *     idValue: int,
	 *     width: int,
	 *     idDriver: string,
	 * } $response API response body
	 * @return Quantity Quantity entity
	 */
	public static function fromApiResponse(array $response): self {
		$unit = $response['unit'];
		if ($unit === '' || $unit === '?') {
			$unit = null;
		}
		return new self(
			id: $response['id'],
			name: $response['name'],
			iqrfName: $response['iqrfName'],
			shortName: $response['shortName'],
			unit: $unit,
			decimalPlaces: $response['decimalPlaces'],
			frcs: $response['frcs'],
			helpPage: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/' . $response['helpPage'],
			idValue: $response['idValue'],
			width: $response['width'],
			idDriver: $response['idDriver'],
		);
	}

	/**
	 * Serializes the Quantity entity into JSON
	 * @return array{
	 *     id: string,
	 *     name: string,
	 *     iqrfName: string,
	 *     shortName: string,
	 *     unit: string|null,
	 *     decimalPlaces: int,
	 *     frcs: array<int>,
	 *     helpPage: string,
	 *     idValue: int,
	 *     width: int,
	 *     idDriver: string,
	 * } JSON serialized Quantity entity
	 */
	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'iqrfName' => $this->iqrfName,
			'shortName' => $this->shortName,
			'unit' => $this->unit,
			'decimalPlaces' => $this->decimalPlaces,
			'frcs' => $this->frcs,
			'helpPage' => $this->helpPage,
			'idValue' => $this->idValue,
			'width' => $this->width,
			'idDriver' => $this->idDriver,
		];
	}

}
