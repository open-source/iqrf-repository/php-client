<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * IQRF OS or DPA attributes entity
 */
final class OsDpaAttributes implements JsonSerializable {

	/**
	 * Mask for beta versions
	 */
	private const MASK_BETA = 0b1;

	/**
	 * Mask for obsolete versions
	 */
	private const MASK_OBSOLETE = 0b10;

	/**
	 * Constructor
	 * @param bool|null $beta Beta version(s)
	 * @param bool|null $obsolete Obsolete version(s)
	 */
	public function __construct(
		public readonly ?bool $beta = null,
		public readonly ?bool $obsolete = null,
	) {
	}

	/**
	 * Creates IQRF OS or DPA attributes object from an integer
	 * @param int $attributes Integer format
	 * @return static IQRF OS or DPA attributes
	 */
	public static function fromInt(int $attributes, int $mask = PHP_INT_MAX): self {
		$beta = ($mask & self::MASK_BETA) !== 0 ? ($attributes & self::MASK_BETA) !== 0 : null;
		$obsolete = ($mask & self::MASK_OBSOLETE) !== 0 ? ($attributes & self::MASK_OBSOLETE) !== 0 : null;
		return new self($beta, $obsolete);
	}

	/**
	 * Serializes the IQRF OS or DPA attributes entity into query parameters
	 * @return array{
	 *     beta?: 'true'|'false',
	 *     obsolete?: 'true'|'false',
	 * } Query parameters
	 */
	public function toQueryParameters(): array {
		$parameters = [];
		if ($this->beta !== null) {
			$parameters['beta'] = $this->beta ? 'true' : 'false';
		}
		if ($this->obsolete !== null) {
			$parameters['obsolete'] = $this->obsolete ? 'true' : 'false';
		}
		return $parameters;
	}

	/**
	 * Serializes the IQRF OS or DPA attributes entity into JSON
	 * @return array{
	 *     beta: bool|null,
	 *     obsolete: bool|null,
	 * } JSON serialized IQRF OS or DPA attributes entity
	 */
	public function jsonSerialize(): array {
		return [
			'beta' => $this->beta,
			'obsolete' => $this->obsolete,
		];
	}

}
