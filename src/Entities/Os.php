<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\TransceiverFamily;
use JsonSerializable;

/**
 * IQRF OS version entity
 */
final class Os implements JsonSerializable {

	/**
	 * Constructor
	 * @param string $build IQRF OS build
	 * @param string $version IQRF OS version
	 * @param TransceiverFamily $trFamily Transceiver family
	 * @param OsDpaAttributes $attributes Attributes
	 * @param string $downloadPath Download path
	 */
	public function __construct(
		public readonly string $build,
		public readonly string $version,
		public readonly TransceiverFamily $trFamily,
		public readonly OsDpaAttributes $attributes,
		public readonly string $downloadPath,
	) {
	}

	/**
	 * Creates a new IQRF OS entity from the API response
	 * @param array{
	 *     build: string,
	 *     version: string,
	 *     trFamily: string,
	 *     attributes: int,
	 *     downloadPath: string,
	 * } $response API response body
	 * @return Os Created IQRF OS entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			build: $response['build'],
			version: $response['version'],
			trFamily: TransceiverFamily::from($response['trFamily']),
			attributes: OsDpaAttributes::fromInt($response['attributes']),
			downloadPath: $response['downloadPath'],
		);
	}

	/**
	 * Serialized the IQRF OS entity into JSON
	 * @return array{
	 *     build: string,
	 *     version: string,
	 *     trFamily: string,
	 *     attributes: array{
	 *         beta: bool|null,
	 *         obsolete: bool|null,
	 *     },
	 *     downloadPath: string,
	 * } JSON serialized IQRF OS entity
	 */
	public function jsonSerialize(): array {
		return [
			'build' => $this->build,
			'version' => $this->version,
			'trFamily' => $this->trFamily->toString(),
			'attributes' => $this->attributes->jsonSerialize(),
			'downloadPath' => $this->downloadPath,
		];
	}

}
