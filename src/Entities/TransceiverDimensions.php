<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Exceptions\TransceiverDimensionsInvalid;
use Nette\Utils\Strings;

/**
 * Transceiver dimensions entity
 */
final class TransceiverDimensions implements \JsonSerializable {

	/**
	 * Constructor
	 * @param float $width Width [mm]
	 * @param float $height Height [mm]
	 * @param float $depth Depth [mm]
	 */
	public function __construct(
		public readonly float $width,
		public readonly float $height,
		public readonly float $depth,
	) {
	}

	/**
	 * Creates a new TransceiverDimensions entity from API Response
	 * @param array{
	 *     dimensions: string
	 * } $response API response body
	 * @return TransceiverDimensions TransceiverDimensions entity
	 * @throws TransceiverDimensionsInvalid Thrown if API response body contains invalid transceiver type dimensions value
	 */
	public static function fromApiResponse(array $response): self {
		$match = Strings::match($response['dimensions'], '/^(?<width>(\d+\.)?\d+)×(?<height>(\d+\.)?\d+)×(?<depth>(\d+\.)?\d+)$/');
		if ($match === null) {
			throw new TransceiverDimensionsInvalid('Invalid transceiver dimensions value: ' . $response['dimensions']);
		}
		return new self(
			width: floatval($match['width']),
			height: floatval($match['height']),
			depth: floatval($match['depth']),
		);
	}

	/**
	 * Serializes the TransceiverDimensions entity into JSON
	 * @return array{
	 *     width: float,
	 *     height: float,
	 *     depth: float,
	 * } JSON serialized TransceiverDimensions entity
	 */
	public function jsonSerialize(): array {
		return [
			'width' => $this->width,
			'height' => $this->height,
			'depth' => $this->depth,
		];
	}

}
