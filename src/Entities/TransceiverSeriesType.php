<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\TransceiverAntenna;

/**
 * Transceiver series type entity
 */
final class TransceiverSeriesType implements \JsonSerializable {

	/**
	 * Constructor
	 * @param string $name Type name
	 * @param bool $temperatureSensor Temperature sensor present
	 * @param TransceiverAntenna $antenna Antenna
	 * @param TransceiverDimensions $dimensions Dimensions
	 * @param string|null $imageUrl Image URL
	 */
	public function __construct(
		public readonly string $name,
		public readonly bool $temperatureSensor,
		public readonly TransceiverAntenna $antenna,
		public readonly TransceiverDimensions $dimensions,
		public readonly string|null $imageUrl = null,
	) {
	}

	/**
	 * Creates a new TransceiverSeriesType entity from API Response
	 * @param array{
	 *     typeName: string,
	 *     temperatureSensor: bool,
	 *     antenna: string,
	 *     dimensions: string,
	 *     image?: string,
	 * } $response API response body
	 * @return TransceiverSeriesType TransceiverSeriesType entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			name: $response['typeName'],
			temperatureSensor: $response['temperatureSensor'],
			antenna: TransceiverAntenna::from($response['antenna']),
			dimensions: TransceiverDimensions::fromApiResponse($response),
			imageUrl: $response['image'] ?? null,
		);
	}

	/**
	 * Serializes the TransceiverSeriesType entity into JSON
	 * @return array{
	 *     name: string,
	 *     temperatureSensor: bool,
	 *     antenna: string,
	 *     dimensions: array{
	 *         width: float,
	 *         height: float,
	 *         depth: float,
	 *     },
	 *     imageUrl: string|null,
	 * } JSON serialized TransceiverSeriesType entity
	 */
	public function jsonSerialize(): array {
		return [
			'name' => $this->name,
			'temperatureSensor' => $this->temperatureSensor,
			'antenna' => $this->antenna->value,
			'dimensions' => $this->dimensions->jsonSerialize(),
			'imageUrl' => $this->imageUrl,
		];
	}

}
