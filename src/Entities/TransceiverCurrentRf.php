<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver RF current entity
 */
final class TransceiverCurrentRf implements \JsonSerializable {

	/**
	 * Constructor
	 * @param TransceiverCurrentRfTx $tx RF TX current
	 * @param TransceiverCurrentRfRx $rx RF RX current
	 * @param float $ready RF ready current [µA]
	 */
	public function __construct(
		public readonly TransceiverCurrentRfTx $tx,
		public readonly TransceiverCurrentRfRx $rx,
		public readonly float $ready,
	) {
	}

	/**
	 * Creates a new TransceiverCurrentRf entity from API Response
	 * @param array{
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 *     currentRfReady: float,
	 * } $response API response body
	 * @return TransceiverCurrentRf TransceiverCurrentRf entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			tx: TransceiverCurrentRfTx::fromApiResponse($response),
			rx: TransceiverCurrentRfRx::fromApiResponse($response),
			ready: $response['currentRfReady'],
		);
	}

	/**
	 * Serializes the TransceiverCurrentRf entity into JSON
	 * @return array{
	 *     rx: array{
	 *         std: float,
	 *         lp: float,
	 *         xlp: float,
	 *     },
	 *     tx: array{
	 *         min: float,
	 *         max: float,
	 *     },
	 *     ready: float,
	 * } JSON serialized TransceiverCurrentRf entity
	 */
	public function jsonSerialize(): array {
		return [
			'rx' => $this->rx->jsonSerialize(),
			'tx' => $this->tx->jsonSerialize(),
			'ready' => $this->ready,
		];
	}

}
