<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver RF RX current entity
 */
final class TransceiverCurrentRfRx implements \JsonSerializable {

	/**
	 * Constructor
	 * @param float $std RF STD-RX current [µA]
	 * @param float $lp RF LP-RX current [µA]
	 * @param float $xlp RF XLP-RX current [µA]
	 */
	public function __construct(
		public readonly float $std,
		public readonly float $lp,
		public readonly float $xlp,
	) {
	}

	/**
	 * Creates a new TransceiverCurrentRfRx entity from API Response
	 * @param array{
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 * } $response API response body
	 * @return TransceiverCurrentRfRx TransceiverCurrentRfRx entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			std: $response['currentRfRxStd'],
			lp: $response['currentRfRxLp'],
			xlp: $response['currentRfRxXlp'],
		);
	}

	/**
	 * Serializes the TransceiverCurrentRfRx entity into JSON
	 * @return array{
	 *     std: float,
	 *     lp: float,
	 *     xlp: float,
	 * } JSON serialized TransceiverCurrentRfRx entity
	 */
	public function jsonSerialize(): array {
		return [
			'std' => $this->std,
			'lp' => $this->lp,
			'xlp' => $this->xlp,
		];
	}

}
