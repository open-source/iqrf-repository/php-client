<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * Company entity
 */
final class Company implements JsonSerializable {

	/**
	 * Constructor
	 * @param int $id Company ID
	 * @param string $name Company name
	 * @param string $homepage Company homepage
	 */
	public function __construct(
		public readonly int $id,
		public readonly string $name,
		public readonly string $homepage,
	) {
	}

	/**
	 * Creates a new company entity from the API response
	 * @param array{
	 *     companyID: int,
	 *     name: string,
	 *     homePage: string,
	 * } $response API response body
	 * @return Company Created company entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			id: $response['companyID'],
			name: $response['name'],
			homepage: $response['homePage'],
		);
	}

	/**
	 * Serializes the Company entity into JSON
	 * @return array{
	 *     id: int,
	 *     name: string,
	 *     homepage: string,
	 * } JSON serialized Company entity
	 */
	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'homepage' => $this->homepage,
		];
	}

}
