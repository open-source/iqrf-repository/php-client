<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver components entity
 */
final class TransceiverComponents implements \JsonSerializable {

	/**
	 * Constructor
	 * @param string $rfChip RF chip identifier
	 * @param string $ldo LDO chip identifier
	 * @param string $temperatureSensorChip Temperature sensor chip identifier
	 */
	public function __construct(
		public readonly string $rfChip,
		public readonly string $ldo,
		public readonly string $temperatureSensorChip
	) {
	}

	/**
	 * Creates a new TransceiverComponents entity from API Response
	 * @param array{
	 *     rfChip: string,
	 *     ldo: string,
	 *     temperatureSensorChip: string,
	 * } $response API response body
	 * @return TransceiverComponents TransceiverComponents entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			rfChip: $response['rfChip'],
			ldo: $response['ldo'],
			temperatureSensorChip: $response['temperatureSensorChip'],
		);
	}

	/**
	 * Serializes the TransceiverComponents entity into JSON
	 * @return array{
	 *     rfChip: string,
	 *     ldo: string,
	 *     temperatureSensorChip: string,
	 * } JSON serialized TransceiverComponents entity
	 */
	public function jsonSerialize(): array {
		return [
			'rfChip' => $this->rfChip,
			'ldo' => $this->ldo,
			'temperatureSensorChip' => $this->temperatureSensorChip,
		];
	}

}
