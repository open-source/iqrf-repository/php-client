<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * Manufacturer entity
 */
final class Manufacturer implements JsonSerializable {

	/**
	 * Constructor
	 * @param int $id Manufacturer name
	 * @param int $companyId Company ID
	 * @param string $name Manufacturer name
	 */
	public function __construct(
		public readonly int $id,
		public readonly int $companyId,
		public readonly string $name,
	) {
	}

	/**
	 * Creates a new manufacturer entity from the API response
	 * @param array{
	 *     manufacturerID: int,
	 *     companyID: int,
	 *     name: string,
	 * } $response API response body
	 * @return Manufacturer Created manufacturer entity from the API response
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			id: $response['manufacturerID'],
			companyId: $response['companyID'],
			name: $response['name'],
		);
	}

	/**
	 * Serializes the Manufacturer entity into JSON
	 * @return array{
	 *     id: int,
	 *     name: string,
	 *     companyId: int,
	 * } JSON serialized Manufacturer entity
	 */
	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'companyId' => $this->companyId,
		];
	}

}
