<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * Detailed IQRF Standard entity
 */
final class StandardDetail implements JsonSerializable {

	/**
	 * Constructor
	 * @param int $id Standard ID
	 * @param string $name Standard name
	 * @param float $version Standard version
	 * @param int $versionFlags Flags for this version
	 * @param string $driver Driver code
	 * @param string $notes Notes
	 */
	public function __construct(
		public readonly int $id,
		public readonly string $name,
		public readonly float $version,
		public readonly int $versionFlags,
		public readonly string $driver,
		public readonly string $notes,
	) {
	}

	/**
	 * Creates an new detailed IQRF Standard entity from the API response
	 * @param array{
	 *     standardID: int,
	 *     name: string,
	 *     version: float,
	 *     versionFlags: int,
	 *     driver: string,
	 *     notes: string,
	 * } $response API response body
	 * @return StandardDetail Created detailed IQRF Standard entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			id: $response['standardID'],
			name: $response['name'],
			version: $response['version'],
			versionFlags: $response['versionFlags'],
			driver: $response['driver'],
			notes: $response['notes'],
		);
	}

	/**
	 * Serializes the detailed IQRF Standard entity into JSON
	 * @return array{
	 *     id: int,
	 *     name: string,
	 *     version: float,
	 *     versionFlags: int,
	 *     driver: string,
	 *     notes: string,
	 * } JSON serialized detailed IQRF Standard entity
	 */
	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'version' => $this->version,
			'versionFlags' => $this->versionFlags,
			'driver' => $this->driver,
			'notes' => $this->notes,
		];
	}

}
