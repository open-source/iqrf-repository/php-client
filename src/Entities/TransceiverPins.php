<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver pins entity
 */
final class TransceiverPins implements \JsonSerializable {

	/**
	 * Constructor
	 * @param int $total Total pins
	 * @param int $ioPins Number of digital I/O pins
	 * @param int $adInputs Number of analog to digital converter inputs
	 */
	public function __construct(
		public readonly int $total,
		public readonly int $ioPins,
		public readonly int $adInputs,
	) {
	}

	/**
	 * Creates a new TransceiverPins entity from API Response
	 * @param array{
	 *     pins: int,
	 *     ioPins: int,
	 *     adInputs: int,
	 * } $response API response body
	 * @return TransceiverPins TransceiverPins entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			total: $response['pins'],
			ioPins: $response['ioPins'],
			adInputs: $response['adInputs'],
		);
	}

	/**
	 * Serializes the TransceiverPins entity into JSON
	 * @return array{
	 *     total: int,
	 *     ioPins: int,
	 *     adInputs: int,
	 * } JSON serialized TransceiverPins entity
	 */
	public function jsonSerialize(): array {
		return [
			'total' => $this->total,
			'ioPins' => $this->ioPins,
			'adInputs' => $this->adInputs,
		];
	}

}
