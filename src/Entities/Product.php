<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\RfModes;
use Iqrf\Repository\Exceptions\CorruptedData;
use JsonSerializable;
use ValueError;

/**
 * Product entity
 */
final class Product implements JsonSerializable {

	/**
	 * Constructor
	 * @param int $hwpid Product HWPID
	 * @param string $name Product name
	 * @param int $manufacturerId Manufacturer ID
	 * @param string $companyName Company name
	 * @param string $homepage Product homepage
	 * @param string $picture Product picture
	 * @param RfModes $rfMode Product RF mode
	 */
	public function __construct(
		public readonly int $hwpid,
		public readonly string $name,
		public readonly int $manufacturerId,
		public readonly string $companyName,
		public readonly string $homepage,
		public readonly string $picture,
		public readonly RfModes $rfMode,
	) {
	}

	/**
	 * Creates a new product entity from the API response
	 * @param array{
	 *     hwpid: int,
	 *     name: string,
	 *     manufacturerID: int,
	 *     companyName: string,
	 *     homePage: string,
	 *     picture: string,
	 *     rfMode: int,
	 * } $response API response data
	 * @return Product Created product entity
	 * @throws CorruptedData Thrown when the API response data are corrupted
	 */
	public static function fromApiResponse(array $response): self {
		try {
			return new self(
				hwpid: $response['hwpid'],
				name: $response['name'],
				manufacturerId: $response['manufacturerID'],
				companyName: $response['companyName'],
				homepage: $response['homePage'],
				picture: $response['picture'],
				rfMode: RfModes::from($response['rfMode']),
			);
		} catch (ValueError) {
			throw new CorruptedData();
		}
	}

	/**
	 * Serializes the product entity into JSON
	 * @return array{
	 *     hwpid: int,
	 *     name: string,
	 *     manufacturerId: int,
	 *     companyName: string,
	 *     homepage: string,
	 *     picture: string,
	 *     rfMode: string,
	 * } JSON serialized product entity
	 */
	public function jsonSerialize(): array {
		return [
			'hwpid' => $this->hwpid,
			'name' => $this->name,
			'manufacturerId' => $this->manufacturerId,
			'companyName' => $this->companyName,
			'homepage' => $this->homepage,
			'picture' => $this->picture,
			'rfMode' => $this->rfMode->jsonSerialize(),
		];
	}

}
