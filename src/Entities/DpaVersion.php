<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * DPA version entity
 */
final class DpaVersion implements JsonSerializable {

	/**
	 * Constructor
	 * @param string $version Raw DPA version
	 */
	public function __construct(
		private readonly string $version,
	) {
	}

	/**
	 * Returns the raw DPA version
	 * @return string Raw DPA version
	 */
	public function getRaw(): string {
		return $this->version;
	}

	/**
	 * Returns the pretty DPA version
	 * @return string Formatted DPA version
	 */
	public function getPretty(): string {
		$versionInt = hexdec($this->version);
		$major = strval($versionInt >> 8);
		$minor = str_pad(dechex($versionInt & 0xff), 2, '0', STR_PAD_LEFT);
		return $major . '.' . $minor;
	}

	/**
	 * Serialized the DPA version entity into JSON
	 * @return string JSON serialized DPA version
	 */
	public function jsonSerialize(): string {
		return $this->getPretty();
	}

}
