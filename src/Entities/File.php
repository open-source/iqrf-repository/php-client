<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use DateTime;
use JsonSerializable;

/**
 * File entity
 */
final class File implements JsonSerializable {

	/**
	 * @var string File name
	 */
	public readonly string $name;

	/**
	 * Constructor
	 * @param string $name File name
	 * @param int $size File size
	 * @param DateTime $date File creation date
	 * @param string $sha256 SHA256 hash of the file
	 */
	public function __construct(
		string $name,
		public readonly int $size,
		public readonly DateTime $date,
		public readonly string $sha256,
	) {
		$this->name = ltrim($name, './');
	}

	/**
	 * Creates a new file entity from the API response
	 * @param array{
	 *     name: string,
	 *     size: int,
	 *     date: int,
	 *     dateDateTime?: string,
	 *     sha256: string,
	 * } $response API response body
	 * @return File File entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			name: $response['name'],
			size: $response['size'],
			date: (new DateTime())->setTimestamp($response['date']),
			sha256: $response['sha256'],
		);
	}

	/**
	 * Serializes the File entity into JSON
	 * @return array{
	 *     name: string,
	 *     size: int,
	 *     creationDate: string,
	 *     sha256: string,
	 * } JSON serialized File entity
	 */
	public function jsonSerialize(): array {
		return [
			'name' => $this->name,
			'size' => $this->size,
			'creationDate' => $this->date->format('c'),
			'sha256' => $this->sha256,
		];
	}

}
