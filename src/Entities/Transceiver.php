<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\McuTypes;
use Iqrf\Repository\Enums\TransceiverFamily;
use Iqrf\Repository\Enums\TransceiverMount;
use Iqrf\Repository\Enums\TransceiverStatus;
use JsonSerializable;

/**
 * Transceiver entity
 */
final class Transceiver implements JsonSerializable {

	/**
	 * @var TransceiverFamily Transceiver family
	 */
	public readonly TransceiverFamily $trFamily;

	/**
	 * Constructor
	 * @param string $homePage Product homepage
	 * @param TransceiverStatus $status Product status
	 * @param TransceiverMount $mounting Mounting method
	 * @param TrSeries $trSeries TR series
	 * @param McuTypes $mcu MCU type
	 * @param TransceiverComponents $components Transceiver components
	 * @param TransceiverPins $pins Transceiver pins
	 * @param TransceiverVoltage $voltage Transceiver voltages
	 * @param TransceiverCurrent $current Transceiver current
	 * @param TransceiverTemperature $operatingTemperature Operating temperatures
	 * @param array<TransceiverSeriesType> $types Transceiver types
	 */
	public function __construct(
		public readonly string $homePage,
		public readonly TransceiverStatus $status,
		public readonly TransceiverMount $mounting,
		public readonly TrSeries $trSeries,
		public readonly McuTypes $mcu,
		public readonly TransceiverComponents $components,
		public readonly TransceiverPins $pins,
		public readonly TransceiverVoltage $voltage,
		public readonly TransceiverCurrent $current,
		public readonly TransceiverTemperature $operatingTemperature,
		public readonly array $types,
	) {
		$this->trFamily = TransceiverFamily::fromSeriesName($trSeries->series->toString());
	}

	/**
	 * Creates a new Transceiver entity from API response
	 * @param array{
	 *     seriesName: string,
	 *     homePage: string,
	 *     status: string,
	 *     mounting: string,
	 *     seriesMIcode: int,
	 *     mcuMIcode: int,
	 *     mcu: string,
	 *     rfChip: string,
	 *     ldo: string,
	 *     temperatureSensorChip: string,
	 *     pins: int,
	 *     ioPins: int,
	 *     adInputs: int,
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 *     supplyVoltageMin: float,
	 *     supplyVoltageMax: float,
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 *     currentRfReady: float,
	 *     currentLEDG: float,
	 *     currentLEDR: float,
	 *     currentMcu: float,
	 *     currentSleep: float,
	 *     currentDeepSleep: float,
	 *     operatingTemperatureMin: float,
	 *     operatingTemperatureMax: float,
	 *     seriesImage: string,
	 *     types: array<array{
	 *         typeName: string,
	 *         temperatureSensor: bool,
	 *         antenna: string,
	 *         dimensions: string,
	 *         image?: string,
	 *     }>
	 * } $response API response body
	 * @return Transceiver Transceiver entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			homePage: $response['homePage'],
			status: TransceiverStatus::from($response['status']),
			mounting: TransceiverMount::from($response['mounting']),
			trSeries: TrSeries::fromApiResponse($response),
			mcu: McuTypes::from($response['mcuMIcode']),
			components: TransceiverComponents::fromApiResponse($response),
			pins: TransceiverPins::fromApiResponse($response),
			voltage: TransceiverVoltage::fromApiResponse($response),
			current: TransceiverCurrent::fromApiResponse($response),
			operatingTemperature: TransceiverTemperature::fromApiResponse($response),
			types: array_map(static fn (array $typeArr): TransceiverSeriesType => TransceiverSeriesType::fromApiResponse($typeArr), $response['types']),
		);
	}

	/**
	 * Serializes the Transceivers entity into JSON
	 * @return array{
	 *     homePage: string,
	 *     status: string,
	 *     trFamily: string,
	 *     trSeries: array{
	 *         value: int,
	 *         name: string,
	 *         imageUrl: string,
	 *     },
	 *     mounting: string,
	 *     mcu: array{
	 *         value: int,
	 *         name: string,
	 *     },
	 *     components: array{
	 *         rfChip: string,
	 *         ldo: string,
	 *         temperatureSensorChip: string,
	 *     },
	 *     pins: array{
	 *         total: int,
	 *         ioPins: int,
	 *         adInputs: int,
	 *     },
	 *     voltage: array{
	 *         operating: array{
	 *             min: float,
	 *             max: float,
	 *             nominal: float,
	 *         },
	 *         supply: array{
	 *             min: float,
	 *             max: float,
	 *         },
	 *     },
	 *     current: array{
	 *         rf: array{
	 *             tx: array{
	 *                 min: float,
	 *                 max: float,
	 *             },
	 *             rx: array{
	 *                 std: float,
	 *                 lp: float,
	 *                 xlp: float,
	 *             },
	 *             ready: float,
	 *         },
	 *         ledg: float,
	 *         ledr: float,
	 *         mcu: float,
	 *         sleep: float,
	 *         deepSleep: float,
	 *     },
	 *     operatingTemperature: array{
	 *         min: float,
	 *         max: float,
	 *     },
	 *     types: array<array{
	 *         name: string,
	 *         temperatureSensor: bool,
	 *         antenna: string,
	 *         dimensions: array{
	 *             width: float,
	 *             height: float,
	 *             depth: float,
	 *         },
	 *         imageUrl: string|null,
	 *     }>,
	 * } JSON serialized Transceiver entity
	 */
	public function jsonSerialize(): array {
		return [
			'homePage' => $this->homePage,
			'status' => $this->status->value,
			'mounting' => $this->mounting->value,
			'trFamily' => $this->trFamily->value,
			'trSeries' => $this->trSeries->jsonSerialize(),
			'mcu' => $this->mcu->jsonSerialize(),
			'components' => $this->components->jsonSerialize(),
			'pins' => $this->pins->jsonSerialize(),
			'voltage' => $this->voltage->jsonSerialize(),
			'current' => $this->current->jsonSerialize(),
			'operatingTemperature' => $this->operatingTemperature->jsonSerialize(),
			'types' => array_map(static fn (TransceiverSeriesType $type): array => $type->jsonSerialize(), $this->types),
		];
	}

}
