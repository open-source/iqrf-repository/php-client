<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Common\FloatValRange;

/**
 * Transceiver operating voltage entity
 */
final class TransceiverOperatingVoltage extends FloatValRange implements \JsonSerializable {

	/**
	 * Constructor
	 * @param float $min Minimum operating voltage [V]
	 * @param float $max Maximum operating voltage [V]
	 * @param float $nominal Nominal operating voltage [V]
	 */
	public function __construct(
		float $min,
		float $max,
		public readonly float $nominal,
	) {
		parent::__construct($min, $max);
	}

	/**
	 * Creates a new TransceiverOperatingVoltage entity from API Response
	 * @param array{
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 * } $response API Response body
	 * @return TransceiverOperatingVoltage TransceiverOperatingVoltage entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			min: $response['operatingVoltageMin'],
			max: $response['operatingVoltageMax'],
			nominal: $response['operatingVoltageNominal'],
		);
	}

	/**
	 * Serializes the TransceiverOperatingVoltage entity into JSON
	 * @return array{
	 *     min: float,
	 *     max: float,
	 *     nominal: float,
	 * } JSON serialized TransceiverOperatingVoltage entity
	 */
	public function jsonSerialize(): array {
		return [
			'min' => $this->min,
			'max' => $this->max,
			'nominal' => $this->nominal,
		];
	}

}
