<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use DateTime;
use JsonSerializable;

/**
 * Files entity
 */
final class Files implements JsonSerializable {

	/**
	 * Constructor
	 * @param DateTime $timestamp Timestamp
	 * @param int $version Format version
	 * @param array<File> $files Array of files
	 */
	public function __construct(
		public readonly DateTime $timestamp,
		public readonly int $version,
		public readonly array $files,
	) {
	}

	/**
	 * Creates a new files entity from the API response
	 * @param array{
	 *     timestamp: int,
	 *     timestampDateTime?: string,
	 *     version: int,
	 *     files: array<array{
	 *         name: string,
	 *         size: int,
	 *         date: int,
	 *         dateDateTime?: string,
	 *         sha256: string
	 *     }>
	 * } $response API response body
	 * @return Files Files entity
	 */
	public static function fromApiResponse(array $response): self {
		$files = array_map(static fn (array $file): File => File::fromApiResponse($file), $response['files']);
		return new self(
			timestamp: (new DateTime())->setTimestamp($response['timestamp']),
			version: $response['version'],
			files: $files,
		);
	}

	/**
	 * Serializes the files entity into JSON
	 * @return array{
	 *     timestamp: string,
	 *     version: int,
	 *     files: array<array{
	 *         name: string,
	 *         size: int,
	 *         creationDate: string,
	 *         sha256: string,
	 *     }>
	 * } JSON serialized files entity
	 */
	public function jsonSerialize(): array {
		return [
			'timestamp' => $this->timestamp->format('c'),
			'version' => $this->version,
			'files' => array_map(static fn (File $file): array => $file->jsonSerialize(), $this->files),
		];
	}

}
