<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver current entity
 */
final class TransceiverCurrent implements \JsonSerializable {

	/**
	 * Constructor
	 * @param TransceiverCurrentRf $rf RF current
	 * @param float $ledg Green LED current [µA]
	 * @param float $ledr Red LED current [µA]
	 * @param float $mcu MCU current [µA]
	 * @param float $sleep Sleep current [µA]
	 * @param float $deepSleep Deep sleep current [µA]
	 */
	public function __construct(
		public readonly TransceiverCurrentRf $rf,
		public readonly float $ledg,
		public readonly float $ledr,
		public readonly float $mcu,
		public readonly float $sleep,
		public readonly float $deepSleep,
	) {
	}

	/**
	 * Creates a new TransceiverCurrent entity from API Response
	 * @param array{
	 *     currentRfTxMin: float,
	 *     currentRfTxMax: float,
	 *     currentRfRxStd: float,
	 *     currentRfRxLp: float,
	 *     currentRfRxXlp: float,
	 *     currentRfReady: float,
	 *     currentLEDG: float,
	 *     currentLEDR: float,
	 *     currentMcu: float,
	 *     currentSleep: float,
	 *     currentDeepSleep: float,
	 * } $response API response body
	 * @return TransceiverCurrent TransceiverCurrent entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			rf: TransceiverCurrentRf::fromApiResponse($response),
			ledg: $response['currentLEDG'],
			ledr: $response['currentLEDR'],
			mcu: $response['currentMcu'],
			sleep: $response['currentSleep'],
			deepSleep: $response['currentDeepSleep'],
		);
	}

	/**
	 * Serializes the TransceiverCurrent entity into JSON
	 * @return array{
	 *     rf: array{
	 *         rx: array{
	 *             std: float,
	 *             lp: float,
	 *             xlp: float,
	 *         },
	 *         tx: array{
	 *             min: float,
	 *             max: float,
	 *         },
	 *         ready: float,
	 *     },
	 *     ledg: float,
	 *     ledr: float,
	 *     mcu: float,
	 *     sleep: float,
	 *     deepSleep: float
	 * } JSON serialized TransceiverCurrent entity
	 */
	public function jsonSerialize(): array {
		return [
			'rf' => $this->rf->jsonSerialize(),
			'ledg' => $this->ledg,
			'ledr' => $this->ledr,
			'mcu' => $this->mcu,
			'sleep' => $this->sleep,
			'deepSleep' => $this->deepSleep,
		];
	}

}
