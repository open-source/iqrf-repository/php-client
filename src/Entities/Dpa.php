<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use JsonSerializable;

/**
 * DPA version entity
 */
final class Dpa implements JsonSerializable {

	/**
	 * Constructor
	 * @param DpaVersion $version DPA version
	 * @param OsDpaAttributes $attributes Attributes
	 * @param string $downloadPath Download path
	 */
	public function __construct(
		public readonly DpaVersion $version,
		public readonly OsDpaAttributes $attributes,
		public readonly string $downloadPath,
	) {
	}

	/**
	 * Creates a new DPA entity from the API response
	 * @param array{
	 *     version: string,
	 *     attributes: int,
	 *     downloadPath: string,
	 * } $response API response body
	 * @return Dpa Created DPA entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			version: new DpaVersion($response['version']),
			attributes: OsDpaAttributes::fromInt($response['attributes']),
			downloadPath: $response['downloadPath'],
		);
	}

	/**
	 * Serialized the DPA entity into JSON
	 * @return array{
	 *     version: string,
	 *     attributes: array{
	 *         beta: bool|null,
	 *         obsolete: bool|null,
	 *     },
	 *     downloadPath: string,
	 * } JSON serialized DPA entity
	 */
	public function jsonSerialize(): array {
		return [
			'version' => $this->version->jsonSerialize(),
			'attributes' => $this->attributes->jsonSerialize(),
			'downloadPath' => $this->downloadPath,
		];
	}

}
