<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

use Iqrf\Repository\Enums\TransceiverFamily;
use JsonSerializable;

/**
 * IQRF OS and DPA entity
 */
final class OsDpa implements JsonSerializable {

	/**
	 * Constructor
	 * @param Os $os IQRF OS
	 * @param Dpa $dpa DPA
	 * @param string $notes Notes
	 */
	public function __construct(
		public readonly Os $os,
		public readonly Dpa $dpa,
		public readonly string $notes,
	) {
	}

	/**
	 * Creates a new IQRF OS and DPA entity from the API response
	 * @param array{
	 *     os: string,
	 *     osVersion: string,
	 *     osTrFamily: string,
	 *     osAttributes: int,
	 *     osDownloadPath: string,
	 *     dpa: string,
	 *     dpaAttributes: int,
	 *     dpaDownloadPath: string,
	 *     notes: string,
	 * } $response API response body
	 * @return OsDpa Created IQRF OS and DPA entity
	 */
	public static function fromApiResponse(array $response): self {
		$os = new Os(
			build: $response['os'],
			version: $response['osVersion'],
			trFamily: TransceiverFamily::from($response['osTrFamily']),
			attributes: OsDpaAttributes::fromInt($response['osAttributes']),
			downloadPath: $response['osDownloadPath'],
		);
		$dpa = new Dpa(
			version: new DpaVersion($response['dpa']),
			attributes: OsDpaAttributes::fromInt($response['dpaAttributes']),
			downloadPath: $response['dpaDownloadPath'],
		);
		return new self(
			os: $os,
			dpa: $dpa,
			notes: $response['notes'],
		);
	}

	/**
	 * Serialized the IQRF OS and DPA entity into JSON
	 * @return array{
	 *     os: array{
	 *         build: string,
	 *         version: string,
	 *         trFamily: string,
	 *         attributes: array{
	 *             beta: bool|null,
	 *             obsolete: bool|null,
	 *         },
	 *         downloadPath: string,
	 *     },
	 *     dpa: array{
	 *         version: string,
	 *         attributes: array{
	 *             beta: bool|null,
	 *             obsolete: bool|null,
	 *         },
	 *         downloadPath: string,
	 *     },
	 *     notes: string,
	 * } JSON serialized IQRF OS and DPA entity
	 */
	public function jsonSerialize(): array {
		return [
			'os' => $this->os->jsonSerialize(),
			'dpa' => $this->dpa->jsonSerialize(),
			'notes' => $this->notes,
		];
	}

}
