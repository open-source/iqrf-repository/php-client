<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Entities;

/**
 * Transceiver voltage entity
 */
final class TransceiverVoltage implements \JsonSerializable {

	/**
	 * Constructor
	 * @param TransceiverOperatingVoltage $operating Operating voltage
	 * @param TransceiverSupplyVoltage $supply Supply voltage
	 */
	public function __construct(
		public readonly TransceiverOperatingVoltage $operating,
		public readonly TransceiverSupplyVoltage $supply,
	) {
	}

	/**
	 * Creates a new TransceiverVoltage entity from API response
	 * @param array{
	 *     operatingVoltageMin: float,
	 *     operatingVoltageMax: float,
	 *     operatingVoltageNominal: float,
	 *     supplyVoltageMin: float,
	 *     supplyVoltageMax: float,
	 * } $response API response body
	 * @return TransceiverVoltage TransceiverVoltage entity
	 */
	public static function fromApiResponse(array $response): self {
		return new self(
			operating: TransceiverOperatingVoltage::fromApiResponse($response),
			supply: TransceiverSupplyVoltage::fromApiResponse($response),
		);
	}

	/**
	 * Serializes the TransceiverVoltage entity into JSON
	 * @return array{
	 *     operating: array{
	 *         min: float,
	 *         max: float,
	 *         nominal: float,
	 *     },
	 *     supply: array{
	 *         min: float,
	 *         max: float,
	 *     },
	 * } JSON serialized TransceiverVoltage entity
	 */
	public function jsonSerialize(): array {
		return [
			'operating' => $this->operating->jsonSerialize(),
			'supply' => $this->supply->jsonSerialize(),
		];
	}

}
