<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Dpa;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DpaNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * DPA versions manager
 */
final class DpaManager extends BaseManager {

	/**
	 * DPA versions path
	 */
	private const PATH = 'dpa';

	/**
	 * Lists all DPA versions
	 * @param OsDpaAttributes|null $attributes DPA version attributes
	 * @return array<Dpa> DPA versions
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws DpaNotFound No DPA versions found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(?OsDpaAttributes $attributes = null): array {
		$query = $attributes?->toQueryParameters() ?? [];
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		$array = $this->parseResponse($response);
		if ($array === []) {
			throw new DpaNotFound();
		}
		return $array;
	}

	/**
	 * Returns DPA version
	 * @param string $version DPA version
	 * @return Dpa DPA version
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws DpaNotFound Requested DPA version not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(string $version): Dpa {
		$query = ['version' => $version];
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		$array = $this->parseResponse($response);
		if ($array === []) {
			throw new DpaNotFound();
		}
		return $array[0];
	}

	/**
	 * Parses the JSON API response
	 * @param array<array{
	 *      version: string,
	 *      attributes: int,
	 *      downloadPath: string,
	 *  }> $response JSON API response
	 * @return array<Dpa> DPA versions
	 */
	private function parseResponse(array $response): array {
		return array_map(
			static fn (array $response): Dpa => Dpa::fromApiResponse($response),
			$response,
		);
	}

}
