<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Os;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\OsNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * IQRF OS versions manager
 */
final class OsManager extends BaseManager {

	/**
	 * IQRF OS versions path
	 */
	private const PATH = 'os';

	/**
	 * Lists all IQRF OS versions
	 * @param OsDpaAttributes|null $attributes IQRF OS version attributes
	 * @return array<Os> IQRF OS versions
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws OsNotFound No IQRF OS versions found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(?OsDpaAttributes $attributes = null): array {
		$query = $attributes?->toQueryParameters() ?? [];
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		$array = $this->parseResponse($response);
		if ($array === []) {
			throw new OsNotFound();
		}
		return $array;
	}

	/**
	 * Returns IQRF OS version
	 * @param string $build IQRF OS build
	 * @return Os IQRF OS version
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws OsNotFound Requested IQRF OS version not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(string $build): Os {
		$query = ['build' => $build];
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		$array = $this->parseResponse($response);
		if ($array === []) {
			throw new OsNotFound();
		}
		return $array[0];
	}

	/**
	 * Parses the JSON API response
	 * @param array<array{
	 *     build: string,
	 *     version: string,
	 *     trFamily: string,
	 *     attributes: int,
	 *     downloadPath: string,
	 * }> $response JSON API response
	 * @return array<Os> IQRF OS versions
	 */
	private function parseResponse(array $response): array {
		return array_map(
			static fn (array $response): Os => Os::fromApiResponse($response),
			$response,
		);
	}

}
