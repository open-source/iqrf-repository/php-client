<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Package;
use Iqrf\Repository\Entities\PackageDetail;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\PackageNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * Package manager
 */
final class PackageManager extends BaseManager {

	/**
	 * Packages path
	 */
	private const PATH = 'packages';

	/**
	 * Lists packages
	 * @param int|null $hwpid HWPID
	 * @param int|null $hwpidVer HWPID version
	 * @param string|null $os IQRF OS build
	 * @param string|null $dpa DPA version
	 * @return array<Package> Array of packages
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws PackageNotFound No packages found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(
		?int $hwpid = null,
		?int $hwpidVer = null,
		?string $os = null,
		?string $dpa = null,
	): array {
		$query = [];
		if ($hwpid !== null) {
			$query['hwpid'] = $hwpid;
		}
		if ($hwpidVer !== null) {
			$query['hwpidVer'] = $hwpidVer;
		}
		if ($os !== null) {
			$query['os'] = $os;
		}
		if ($dpa !== null) {
			$query['dpa'] = $dpa;
		}
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		if ($response === []) {
			throw new PackageNotFound();
		}
		foreach ($response as &$item) {
			$item = Package::fromApiResponse($item);
		}
		return $response;
	}

	/**
	 * Returns the package by its ID
	 * @param int $id Package ID
	 * @return PackageDetail Detailed package entity
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws PackageNotFound Requested package not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(int $id): PackageDetail {
		try {
			$response = $this->apiClient->sendApiRequest(self::PATH . '/' . $id);
		} catch (DataNotFound) {
			throw new PackageNotFound();
		}
		return PackageDetail::fromApiResponse($response);
	}

}
