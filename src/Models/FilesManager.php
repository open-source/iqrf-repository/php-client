<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\File;
use Iqrf\Repository\Entities\Files;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\FileNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;
use Iqrf\Repository\Utils\FileClient;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Files manager
 */
final class FilesManager {

	/**
	 * Constructor
	 * @param FileClient $httpClient HTTP(S) file client
	 * @param bool $useCredentials Use credentials
	 */
	public function __construct(
		private readonly FileClient $httpClient,
		public bool $useCredentials = false,
	) {
	}

	/**
	 * Sets the base path
	 * @param string $path Base path
	 */
	public function setPath(string $path): void {
		$this->httpClient->setPath($path);
	}

	/**
	 * Download the file and returns its content
	 * @param string $fileName File name
	 * @return string|null File content
	 * @throws FileNotFound Requested file not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function download(string $fileName): ?string {
		$file = $this->get($fileName);
		if (!$file instanceof File) {
			return null;
		}
		return $this->httpClient->getFile($file->name, $this->useCredentials);
	}

	/**
	 * Return the file entity
	 * @param string $fileName File name
	 * @return File|null File
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws FileNotFound Requested file not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(string $fileName): ?File {
		$files = $this->list();
		$name = ltrim($fileName, './');
		foreach ($files->files as $file) {
			if ($file->name === $name) {
				return $file;
			}
		}
		return null;
	}

	/**
	 * Lists files
	 * @return Files Files
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws FileNotFound No files found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(): Files {
		try {
			$json = $this->httpClient->getFile('/__files__.json', $this->useCredentials);
			$array = Json::decode($json, forceArrays: true);
			return Files::fromApiResponse($array);
		} catch (DataNotFound) {
			throw new FileNotFound();
		} catch (JsonException) {
			throw new CorruptedData();
		}
	}

}
