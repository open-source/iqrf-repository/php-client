<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Standard;
use Iqrf\Repository\Entities\StandardDetail;
use Iqrf\Repository\Entities\StandardVersions;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;
use Iqrf\Repository\Exceptions\StandardNotFound;

/**
 * IQRF Standards manager
 */
final class StandardManager extends BaseManager {

	/**
	 * IQRF Standards path
	 */
	private const PATH = 'standards';

	/**
	 * Lists all IQRF Standards
	 * @return array<Standard> All IQRF Standards
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(): array {
		return array_map(
			fn (array $item): Standard => Standard::fromApiResponse($item),
			$this->apiClient->sendApiRequest(self::PATH),
		);
	}

	/**
	 * Returns IQRF Standard versions
	 * @param int $id IQRF Standard ID
	 * @return StandardVersions IQRF Standard versions
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 * @throws StandardNotFound IQRF Standard not found
	 */
	public function getVersions(int $id): StandardVersions {
		try {
			$response = $this->apiClient->sendApiRequest(self::PATH . '/' . $id);
		} catch (DataNotFound) {
			throw new StandardNotFound();
		}
		return StandardVersions::fromApiResponse($response);
	}

	/**
	 * Returns IQRF Standard
	 * @param int $id IQRF Standard ID
	 * @param float $version IQRF Standard version
	 * @return StandardDetail IQRF Standard
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 * @throws StandardNotFound IQRF Standard not found
	 */
	public function get(int $id, float $version): StandardDetail {
		try {
			$response = $this->apiClient->sendApiRequest(self::PATH . '/' . $id . '/' . $version);
		} catch (DataNotFound) {
			throw new StandardNotFound();
		}
		return StandardDetail::fromApiResponse($response);
	}

}
