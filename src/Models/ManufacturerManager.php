<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Manufacturer;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ManufacturerNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * Manufacturer manager
 */
final class ManufacturerManager extends BaseManager {

	/**
	 * Manufacturers path
	 */
	private const PATH = 'manufacturers';

	/**
	 * Lists all manufacturers
	 * @return array<Manufacturer> All manufacturers
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(): array {
		return array_map(
			fn (array $item): Manufacturer => Manufacturer::fromApiResponse($item),
			$this->apiClient->sendApiRequest(self::PATH),
		);
	}

	/**
	 * Returns the manufacturer by its ID
	 * @param int $id Manufacturer
	 * @return Manufacturer Manufacturer
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ManufacturerNotFound Requested manufacturer not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(int $id): Manufacturer {
		try {
			$response = $this->apiClient->sendApiRequest(self::PATH . '/' . $id);
		} catch (DataNotFound) {
			throw new ManufacturerNotFound();
		}
		return Manufacturer::fromApiResponse($response);
	}

}
