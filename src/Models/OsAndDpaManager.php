<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\OsDpa;
use Iqrf\Repository\Entities\OsDpaAttributes;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\ServiceUnavailable;
use Nette\Utils\Strings;

/**
 * IQRF OS and DPA versions manager
 */
final class OsAndDpaManager extends BaseManager {

	/**
	 * IQRF OS and DPA versions path
	 */
	private const PATH = 'osdpa';

	/**
	 * Lists all IQRF OS and DPA versions
	 * @param OsDpaAttributes|null $attributes IQRF OS and DPA version attributes
	 * @return array<OsDpa> IQRF OS and DPA versions
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(?OsDpaAttributes $attributes = null): array {
		$query = $attributes !== null ? $this->getQueryAttributes($attributes) : [];
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		return $this->parseResponse($response);
	}

	/**
	 * Returns filtered IQRF OS and DPA versions
	 * @param string|null $os IQRF OS build
	 * @param string|null $dpa DPA versions
	 * @param OsDpaAttributes|null $attributes IQRF OS and DPA version attributes
	 * @return array<OsDpa> IQRF OS and DPA versions
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(?string $os = null, ?string $dpa = null, ?OsDpaAttributes $attributes = null): array {
		$query = $attributes !== null ? $this->getQueryAttributes($attributes) : [];
		if ($os !== null) {
			$query['os'] = $os;
		}
		if ($dpa !== null) {
			$query['dpa'] = $dpa;
		}
		$response = $this->apiClient->sendApiRequest(self::PATH, $query);
		return $this->parseResponse($response);
	}

	/**
	 * Returns IQRF OS and DPA attributes as query parameters
	 * @param OsDpaAttributes $attributes IQRF OS and DPA attributes
	 * @return array<string, string> Query parameters
	 */
	private function getQueryAttributes(OsDpaAttributes $attributes): array {
		$parameters = $attributes->toQueryParameters();
		$query = [];
		foreach ($parameters as $key => $value) {
			$key = Strings::capitalize($key);
			$query['os' . $key] = $value;
			$query['dpa' . $key] = $value;
		}
		return $query;
	}

	/**
	 * Parses the JSON API response
	 * @param array<array{
	 *     os: string,
	 *     osVersion: string,
	 *     osTrFamily: string,
	 *     osAttributes: int,
	 *     osDownloadPath: string,
	 *     dpa: string,
	 *     dpaAttributes: int,
	 *     dpaDownloadPath: string,
	 *     notes: string,
	 * }> $response JSON API response
	 * @return array<OsDpa> IQRF OS and DPA versions
	 */
	private function parseResponse(array $response): array {
		return array_map(
			static fn (array $response): OsDpa => OsDpa::fromApiResponse($response),
			$response,
		);
	}

}
