<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Models;

use Iqrf\Repository\Entities\Product;
use Iqrf\Repository\Exceptions\CorruptedData;
use Iqrf\Repository\Exceptions\DataNotFound;
use Iqrf\Repository\Exceptions\ProductNotFound;
use Iqrf\Repository\Exceptions\ServiceUnavailable;

/**
 * Product manager
 */
final class ProductManager extends BaseManager {

	/**
	 * Products path
	 */
	private const PATH = 'products';

	/**
	 * Lists all products
	 * @return array<Product> All products
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function list(): array {
		return array_map(
			static fn (array $item): Product => Product::fromApiResponse($item),
			$this->apiClient->sendApiRequest(self::PATH),
		);
	}

	/**
	 * Returns the product by its HWPID
	 * @param int $hwpid Product HWPID
	 * @return Product Product
	 * @throws CorruptedData Corrupted data in the API response
	 * @throws ProductNotFound Requested product not found
	 * @throws ServiceUnavailable IQRF Repository API is unavailable
	 */
	public function get(int $hwpid): Product {
		try {
			$response = $this->apiClient->sendApiRequest(self::PATH . '/' . $hwpid);
		} catch (DataNotFound) {
			throw new ProductNotFound();
		}
		return Product::fromApiResponse($response);
	}

}
