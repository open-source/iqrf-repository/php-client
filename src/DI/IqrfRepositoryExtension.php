<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\DI;

use Iqrf\Repository\Configuration;
use Iqrf\Repository\Entities\Credentials;
use Iqrf\Repository\Models\CompanyManager;
use Iqrf\Repository\Models\FilesManager;
use Iqrf\Repository\Models\ManufacturerManager;
use Iqrf\Repository\Models\OsAndDpaManager;
use Iqrf\Repository\Models\PackageManager;
use Iqrf\Repository\Models\ProductManager;
use Iqrf\Repository\Models\QuantityManager;
use Iqrf\Repository\Models\ServerManager;
use Iqrf\Repository\Models\StandardManager;
use Iqrf\Repository\Models\TransceiverManager;
use Iqrf\Repository\Utils\ApiClient;
use Iqrf\Repository\Utils\FileClient;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use stdClass;

/**
 * Nette DI extension for IQRF Repository API integration
 * @property stdClass $config Configuration
 */
class IqrfRepositoryExtension extends CompilerExtension {

	/**
	 * Returns the configuration schema
	 * @return Schema Configuration schema
	 */
	public function getConfigSchema(): Schema {
		return Expect::structure([
			'apiEndpoint' => Expect::string()
				->default('https://repository.iqrfalliance.org/api')
				->dynamic(),
			'credentials' => Expect::structure([
				'username' => Expect::anyOf(Expect::null(), Expect::string())
					->firstIsDefault()
					->dynamic(),
				'password' => Expect::anyOf(Expect::null(), Expect::string())
					->firstIsDefault()
					->dynamic(),
			]),
		]);
	}

	/**
	 * Loads extension configuration
	 */
	public function loadConfiguration(): void {
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('credentials'))
			->setType(Credentials::class)
			->setArguments([$this->config->credentials->username, $this->config->credentials->password]);

		$builder->addDefinition($this->prefix('config'))
			->setType(Configuration::class)
			->setArguments([$this->config->apiEndpoint]);

		$builder->addDefinition($this->prefix('apiClient'))
			->setType(ApiClient::class)
			->setArguments(['@' . $this->prefix('config')]);

		$builder->addDefinition($this->prefix('fileClient'))
			->setType(FileClient::class)
			->setArguments(['@' . $this->prefix('config')]);

		$builder->addDefinition($this->prefix('filesManager'))
			->setType(FilesManager::class);

		$builder->addDefinition($this->prefix('companyManager'))
			->setType(CompanyManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('manufacturerManager'))
			->setType(ManufacturerManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('osDpaManager'))
			->setType(OsAndDpaManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('packageManager'))
			->setType(PackageManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('productManager'))
			->setType(ProductManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('quantityManager'))
			->setType(QuantityManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('serverManager'))
			->setType(ServerManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('standardManager'))
			->setType(StandardManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);

		$builder->addDefinition($this->prefix('transceiverManager'))
			->setType(TransceiverManager::class)
			->setArguments(['@' . $this->prefix('apiClient')]);
	}

}
