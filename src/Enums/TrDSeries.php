<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

/**
 * TR-D series enum
 */
enum TrDSeries: int implements ITrSeries {

	/// TR-52D
	case TR_52D = 0;
	/// TR-58D-RJ
	case TR_58D_RJ = 1;
	/// TR-72D
	case TR_72D = 2;
	/// TR-53D
	case TR_53D = 3;
	/// TR-78D
	case TR_78D = 4;
	/// TR-54D
	case TR_54D = 8;
	/// TR-55D
	case TR_55D = 9;
	/// TR-56D
	case TR_56D = 10;
	/// TR-76D
	case TR_76D = 11;
	/// TR-77D
	case TR_77D = 12;
	/// TR-75D
	case TR_75D = 13;

	/**
	 * {@inheritDoc}
	 */
	public function toString(): string {
		return match ($this) {
			self::TR_52D => 'TR-52D',
			self::TR_58D_RJ => 'TR-58D-RJ',
			self::TR_72D => 'TR-72D',
			self::TR_53D => 'TR-53D',
			self::TR_78D => 'TR-78D',
			self::TR_54D => 'TR-54D',
			self::TR_55D => 'TR-55D',
			self::TR_56D => 'TR-56D',
			self::TR_76D => 'TR-76D',
			self::TR_77D => 'TR-77D',
			self::TR_75D => 'TR-75D',
		};
	}

	/**
	 * Converts the TR-D series to a string
	 * @return string TR-D series as a string
	 */
	public function jsonSerialize(): string {
		return $this->toString();
	}

}
