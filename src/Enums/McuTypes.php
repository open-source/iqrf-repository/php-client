<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

use JsonSerializable;

/**
 * MCU types enum
 */
enum McuTypes: int implements JsonSerializable {

	/// PIC16F886 microcontroller
	case PIC16F886 = 3;
	/// PIC16LF1938 microcontroller
	case PIC16LF1938 = 4;
	/// PIC16LF18877 microcontroller
	case PIC16LF18877 = 5;

	/**
	 * Converts the RF mode to a string
	 * @return string RF mode as a string
	 */
	public function toString(): string {
		return match ($this) {
			self::PIC16F886 => 'PIC16F886',
			self::PIC16LF1938 => 'PIC16LF1938',
			self::PIC16LF18877 => 'PIC16LF18877',
		};
	}

	/**
	 * Converts the MCU type to a string
	 * @return array{
	 *     value: int,
	 *     name: string,
	 * } MCU type as a string
	 */
	public function jsonSerialize(): array {
		return [
			'value' => $this->value,
			'name' => $this->toString(),
		];
	}

}
