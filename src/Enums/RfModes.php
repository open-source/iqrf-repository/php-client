<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

/**
 * RF modes enum
 */
enum RfModes: int implements \JsonSerializable {

	/// STD RF mode
	case STD = 1;
	/// LP RF mode
	case LP = 2;
	/// STD+LP RF mode
	case STD_LP = 3;

	/**
	 * Converts the RF mode to a string
	 * @return string RF mode as a string
	 */
	public function toString(): string {
		return match ($this) {
			self::STD => 'STD',
			self::LP => 'LP',
			self::STD_LP => 'STD+LP',
		};
	}

	/**
	 * Converts the RF mode to a string
	 * @return string RF mode as a string
	 */
	public function jsonSerialize(): string {
		return $this->toString();
	}

}
