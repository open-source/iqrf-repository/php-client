<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

/**
 * TR-G series enum
 */
enum TrGSeries: int implements ITrSeries {

	/// TR-82G
	case TR_82G = 0;
	/// TR-72G
	case TR_72G = 2;
	/// TR-85G
	case TR_85G = 9;
	/// TR-86G
	case TR_86G = 10;
	/// TR-76G
	case TR_76G = 11;
	/// TR-75G
	case TR_75G = 13;

	/**
	 * {@inheritDoc}
	 */
	public function toString(): string {
		return match ($this) {
			self::TR_82G => 'TR-82G',
			self::TR_72G => 'TR-72G',
			self::TR_85G => 'TR-85G',
			self::TR_86G => 'TR-86G',
			self::TR_76G => 'TR-76G',
			self::TR_75G => 'TR-75G',
		};
	}

	/**
	 * Converts the TR-G series to a string
	 * @return string TR-G series as a string
	 */
	public function jsonSerialize(): string {
		return $this->toString();
	}

}
