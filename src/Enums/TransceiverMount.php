<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

/**
 * Transceiver mount enum
 */
enum TransceiverMount: string {

	/// RJ-45 connector
	case RJ45 = 'RJ-45';
	/// SIM mount
	case SIM = 'SIM';
	/// SMT (surface-mount technology) mount
	case SMT = 'SMT';
	/// Various mount
	case Various = 'Various';
	/// Vertical mount
	case Vertical = 'Vertical';

	/**
	 * Vertical mount
	 * @deprecated Use Vertical instead
	 */
	public const VERTICAL = self::Vertical;

}
