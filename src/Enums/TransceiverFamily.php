<?php

/**
 * Copyright 2019-2024 IQRF Tech s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
declare(strict_types = 1);

namespace Iqrf\Repository\Enums;

use Nette\Utils\Strings;
use ValueError;

/**
 * Transceiver family enum
 */
enum TransceiverFamily: string {

	/// TR-5xD family
	case TR_5xD = '5xD';
	/// TR-7xD family
	case TR_7xD = '7xD';
	/// TR-7xG family
	case TR_7xG = '7xG';
	// TR-8xG family
	case TR_8xG = '8xG';

	/**
	 * Creates a transceiver family from a transceiver series name
	 * @param string $seriesName Transceiver series name
	 * @return TransceiverFamily Transceiver family
	 */
	public static function fromSeriesName(string $seriesName): self {
		if (Strings::match($seriesName, '/^TR-5\dD/') !== null) {
			return self::TR_5xD;
		}
		if (Strings::match($seriesName, '/^TR-7\dD/') !== null) {
			return self::TR_7xD;
		}
		if (Strings::match($seriesName, '/^TR-7\dG/') !== null) {
			return self::TR_7xG;
		}
		if (Strings::match($seriesName, '/^TR-8\dG/') !== null) {
			return self::TR_8xG;
		}
		throw new ValueError('Unknown TR series name');
	}

	/**
	 * Converts the transceiver family to a string
	 * @return string Transceiver family
	 */
	public function toString(): string {
		return match ($this) {
			self::TR_5xD => 'TR-5xD',
			self::TR_7xD => 'TR-7xD',
			self::TR_7xG => 'TR-7xG',
			self::TR_8xG => 'TR-8xG',
		};
	}

}
